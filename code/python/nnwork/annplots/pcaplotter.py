'''
Created on Mar 3, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''

import matplotlib
matplotlib.use('Qt4Agg')
from matplotlib.ticker import FuncFormatter
from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np
import matplotlib.cm as cm


class ANNPlotter( animation.TimedAnimation ):

    def __init__( self, max_steps, max_coeff = 1, p = 3, title = "", interval = 50 ):
        '''
        ANNPlotter for an ANN using GHA to learn PCA.  This produces an animation 
        for the error over each epoch, and 
        then for each epoch, the bottom subplot will show the learning history 
        of GHA network with respect to I (e.g. eye) and the PCS.  The animation does 
        not update
        during the ANN's execution, and this is mostly due to me not being able 
        to figure out the correct config for Matplotlib on MacOSX for both EPD
        Enthought Python Distribution and the standard Python distribution using
        MacPorts.   
        
        
        max_steps: maximum number of learning steps
        max_coeff: maximum value for all the PC coefficients
        p: number of PCs that are being learned
        title: title to put on the plot
        interval: interval to use for the TimedAnimation
        
        '''
        self.min_error = 100000
        self.step_min_error = -1
        self.max_steps = max_steps
        
        
        self.pos = 0
        
        self.pca_vectors = []
        
        self.inputs = []
        self.results = []
        self.learning_steps = []
        self.errors = []
        self.targets = []
        self.title = title
        self.fig1 = plt.figure(1,figsize=(12,8))
        self.fig1.suptitle(self.title, fontsize = 20)
        

        self.p = p
        self.results = [[], [], [], []]
        self.results_lines = []
        for i in xrange(0, self.p):
            s = r"%d-element in diag($P_{GHA} * P_{GHA}'$)"%i
            self.results_lines.append( plt.Line2D([], [], label=s, color=cm.summer(i*int(200/self.p)),   lw = 3))
        
        
        
        
        self.mags_lines = []
        for i in xrange(0, self.p):
            s = r"%d-element in diag($P_{GHA} * P_{GHA}'$)"%i
            self.mags_lines.append( plt.Line2D([], [], label=s, color=cm.summer(i*int(200/self.p)),   lw = 3))

        
        # code I have taken from various examples
        ## check for flipy bugs
        self.fig_ax1 = self.fig1.add_subplot(211)
        self.fig_ax2 = self.fig1.add_subplot(212)
        

        self.fig_ax1.set_xlim(0, 6)
        self.fig_ax1.set_ylim(-max_coeff, max_coeff)

        for i in self.mags_lines:
            self.fig_ax1.add_line(i)
            
        

        self.fig_ax1.set_xlabel("$i^{th}$ element in the diag($P_{GHA} * P_{GHA}'$)", fontsize = 18)
        self.fig_ax1.set_ylabel(r"Error (${\bf I}$ - P$_{GHA}$ * P$_{GHA}$')", fontsize = 18)
        self.fig_ax1.set_yscale('linear')
        self.fig_ax1.set_clip_on(False)
        self.fig_ax1.legend(shadow=True, fancybox=True)
        
        for i in self.results_lines:
            self.fig_ax2.add_line(i)
        
        
        
        self.fig_ax2.set_xlabel('Learning Step', fontsize = 18)
        self.fig_ax2.set_ylabel(r"Error (${\bf I}$ - P$_{GHA}$ * P$_{GHA}$')", fontsize = 18)
        self.fig_ax2.set_yscale('linear')
        self.fig_ax2.set_xlim(0, max_steps)
        self.fig_ax2.set_ylim(-max_coeff, max_coeff)
        self.fig_ax2.set_clip_on(False) 
        self.fig_ax2.legend(shadow=True, fancybox=True)
        self.fig_ax2.grid(True) 
        self.fig_ax1.grid(True)

        
    
    def init_animation(self, file_name):
        plt.ion()
        plt.show()
        self.fig1.suptitle(self.title, fontsize = 20)
        animation.TimedAnimation.__init__(self, self.fig1, blit=False, interval = 500, repeat = False)
        self.save(file_name)
        
    
    def init(self):
        for i in self.results_lines:
            i.set_data([],[])
        return self.results_lines
            
    
    def update_results( self, pc_coef, learning_step ):
        
        pcas = np.dot(pc_coef, pc_coef.T)
        pcas = pcas - np.eye(pcas.shape[0], pcas.shape[1],dtype=np.float)
        #print pcas.shape
        #print pcas
        
        for i in xrange(0, self.p):
            self.results[i].append(pcas[i,i])
        
        self.learning_steps.append(learning_step)
        
        

    def _draw_frame(self, framedata):
        i = framedata
        print ("Drawing new frame %d, %d"% (i, self.learning_steps[i]))
        
        j = 0
        while j < self.p:
            r = self.results[j]
            self.results_lines[j].set_data(self.learning_steps[:i],r[:i])
            self.mags_lines[j].set_data([j+1, j+1],[0, r[i]])
            j+= 1
        
        self._drawn_artists = [self.results_lines + self.mags_lines]


    

    def new_frame_seq(self):
        return iter(range(len(self.learning_steps)))

    def _init_draw(self):
        for i in self.results_lines:
            i.set_data([],[])
        
        for i in self.mags_lines:
            i.set_data([],[])
