from nnwork.ann.backprop import create_error_historys
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio


def plot_all_errors(X, Y_train, Y_test, Y_test2):
    '''
    plot all three output errors on the same figure with three
    different subplots.  then create plots with the correct
    formula describing the output formula
    
    X: inputs
    Y_train: training error for outputs
    Y_test: test error for outputs
    Y_test2: second set of test error for outputs 
    
    '''
    
    fig, axs = plt.subplots(nrows=3, sharex=True, figsize=(12,8))

    ax = axs[0]
    ax1 = axs[1]
    ax2 = axs[2]
    
    title = '''Point plots of the Test 1, Test 2, and the Training Data
    for a 1-1-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5 '''+\
    '''Learning Steps'''
    
    #fig.legend(("Training", "Test 1", "Test 2"),
    #           loc = 0, shadow=True, fancybox=True)
    
    plt.xlabel('Signal Input',  fontsize=16)
    
    fig.suptitle(title, fontsize = 16)
    
    
    #ax = fig.add_subplot(311)
    ax.text(2, 0.65, r'$sin(\frac{2{\pi}x}{20})$', color='k', label="Training", 
            fontsize=14)
    ax.plot(X,Y_train,'ob--')
    ax.grid(True)
    ax.set_ylabel('Signal Output',  fontsize=16)
    ax.set_xlim(0, 22)
    #ax1 = fig.add_subplot(312)
    ax1.text(2, 0.65, 
             r'$.8sin(\frac{2{\pi}x}{10}) + .25 * cos(\frac{2{\pi}}{25})$', 
             color='k', label="Test 1", fontsize=14)
    ax1.plot(X,Y_test,'ob:')
    ax1.grid(True)
    ax1.set_ylabel('Signal Output',  fontsize=16)
    ax1.set_xlim(0, 22)
    #ax2 = fig.add_subplot(313)
    ax2.text(2, 0.4, r'$Uniform Random Inputs$', color='k', label="Test 2", 
             fontsize=14)
    ax2.plot(X,Y_test2,'ob')
    ax2.grid(True)
    ax2.set_ylabel('Signal Output',  fontsize=16)
    ax2.set_xlim(0, 22)
    #plt.legend([ax, ax1, ax2], ["Training", "Test Set 1", "Test Set 2"], loc=(1.1,0.1)) 
    plt.show()
    
    
def plot_prediction(X, Y_train, Y_test, Y_test2):
    '''
    plot all three output errors on the same figure with three
    different subplots.  then create plots with the correct
    formula describing the output formula
    
    X: inputs
    Y_train: training  outputs
    Y_test: test  outputs
    Y_test2: second set of test outputs 
    
    '''

    # plot the results from the prediction
    fig, axs = plt.subplots(nrows=3, sharex=True, figsize=(12,8))
    ax = axs[0]
    ax1 = axs[1]
    ax2 = axs[2]
    
    title = '''Point plots of the Test 1, Test 2, and the Training Data
    for a 1-1-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5'''+\
    ''' Learning Steps'''
    
    #fig.legend(("Training", "Test 1", "Test 2"),
    #           loc = 0, shadow=True, fancybox=True)
    
    plt.xlabel('Signal Input',  fontsize=16)
    
    fig.suptitle(title, fontsize = 16)
    
    
    #ax = fig.add_subplot(311)
    ax.text(2, 0.65, r'$sin(\frac{2{\pi}x}{20})$', color='k', label="Training", 
            fontsize=14)
    ax.plot(X,Y_train,'ob--')
    ax.grid(True)
    ax.set_ylabel('Signal Output',  fontsize=16)
    ax.set_xlim(0, 22)
    #ax1 = fig.add_subplot(312)
    ax1.text(2, 0.65, 
             r'$.8sin(\frac{2{\pi}x}{10}) + .25 * cos(\frac{2{\pi}}{25})$', 
             color='k', label="Test 1", fontsize=14)
    ax1.plot(X,Y_test,'ob:')
    ax1.grid(True)
    ax1.set_ylabel('Signal Output',  fontsize=16)
    ax1.set_xlim(0, 22)
    #ax2 = fig.add_subplot(313)
    ax2.text(2, 0.4, r'$Uniform Random Inputs$', color='k', label="Test 2", 
             fontsize=14)
    ax2.plot(X,Y_test2,'ob')
    ax2.grid(True)
    ax2.set_ylabel('Signal Output',  fontsize=16)
    ax2.set_xlim(0, 22)
    #plt.legend([ax, ax1, ax2], ["Training", "Test Set 1", "Test Set 2"], loc=(1.1,0.1)) 
    plt.show()



def plot_error_combined(epats, error_t, history, transfer, bias, slope,
                        inputs, scale_up, epoch):
    '''
    plot all three output errors on the same figure with three
    different subplots.  then create plots with the correct
    formula describing the output formula
    
    epats: dict with the named patterns
    error_t: error function used to assess the error of the outputs
    history: weight history used to reproduce the measurements at each 
        saved learning step in the network
    transfer: transfer function used for the neural network
    bias: bias for the 2-layer netowk [bias0, bias1]
    slope: slope of the transfer function
    inputs: inputs used to train the network
    scale_up: scaling function for the outputs
    epoch: epoch (e.g. batch v. online) learning in the network

    
    '''

    ehistory = create_error_historys(error_t, history, transfer, bias, slope, 
                                     inputs, epats, scale_up = scale_up)
    
    epochs = ehistory['test']['epoch']
    testErrors = ehistory['test']['error']
    test2Errors = ehistory['test2']['error']
    trainErrors = ehistory['training']['error']
    
    fig = plt.figure(figsize=(12,8))
    plt.ylabel('Recall Error (RMS)',  fontsize=16)
    title = '''Error History Training versus Test by Epoch\n'''+ \
            '''Configuration = 1-1-1, alpha = 0.015, m = 2000 '''+ \
            '''Learning steps = 1x10^6, Epoch Size = 1'''
            
    fig.suptitle(title, fontsize = 16)
    ax = fig.add_subplot(111)
    ax.set_ylim(0,1.5)
    ax.set_xlim(0,max(epochs))
    ax.set_xlabel('Learning Steps in Epochs, Epoch = %d'%epoch, fontsize=16)
    ax.set_ylabel('Recall Error (RMS)',  fontsize=16)
    ax.plot(epochs[0:], trainErrors[0:], 'ob-',
            epochs[0:], testErrors[0:], 'ok-',
            epochs[0:], test2Errors[0:], 'or-',)
    ax.legend((r'Training: $sin(\frac{2{\pi}x}{20})$', 
               r'Test 1:$.8sin(\frac{2{\pi}x}{10}) + .25 * cos(\frac{2{\pi}}{25})$', 
               r'Test 2: Uniform Random Output'), loc='upper left')
    ax.grid(True)
    #ax.draw()
    plt.show()