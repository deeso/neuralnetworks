'''
Created on Mar 3, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''

import matplotlib
matplotlib.use('Qt4Agg')
from matplotlib.ticker import FuncFormatter
from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np


class ANNPlotter( animation.TimedAnimation ):

    def __init__( self, inputs_range, inputs_yrange, error_range, error_yrange,
                   scale_up = None, title = "", interval = 50 ):
        '''
        ANNPlotter for a scalar input and a scalar output for the neural 
        network.  This produces an animation for the error over each epoch, and 
        then for each epoch, the bottom subplot will show the input values versus
        the actual output values of the network.  The animation does not update
        during the ANN's execution, and this is mostly due to me not being able 
        to figure out the correct config for Matplotlib on MacOSX for both EPD
        Enthought Python Distribution and the standard Python distribution using
        MacPorts.   
        
        
        inputs_range: x-axis of the inputs [min, max] values
        inputs_yrange: y-axis of the inputs [min, max] values
        error_range: x-axis of the inputs [min, max] values
        error_yrange: y-axis of the inputs [min, max] values
        scale_up: function to scale the output properly before capturing the 
            value
        
        '''
        self.min_error = 100000
        self.step_min_error = -1
        self.max_steps = error_range[1]
        self.max_error_repr = error_yrange[1]
        
        self.update_box = plt.Text(self.max_steps / 4 , 
                                   self.max_error_repr - self.max_error_repr * .6, 
                                   text = "No Error Info Reported Yet.", fontsize=18 )

        self.pos = 0
        
        self.inputs = []
        self.results = []
        self.learning_steps = []
        self.errors = []
        self.targets = []
        self.title = title
        self.fig1 = plt.figure(1,figsize=(12,8))
        self.fig1.suptitle(self.title, fontsize = 20)
        
        self.scale_up = scale_up

        log_2_product = lambda x, pos: "%.03f" %(x)
        
        formatter = FuncFormatter(log_2_product)
        
        self.error_line = plt.Line2D([], [], color='red', marker='o', lw = 3)
        self.results_line =  plt.Line2D([], [], color='black', marker='o', lw = 3)
        self.targets_line =  plt.Line2D([], [], color='blue', marker='o', lw = 3)
        # code I have taken from various examples
        ## check for flipy bugs

        
        self.fig_ax1 = self.fig1.add_subplot(211)
        self.fig_ax2 = self.fig1.add_subplot(212)
        
        



        # setting up the limits for the  plots
        self.fig_ax1.set_yscale('log')
        self.fig_ax1.yaxis.set_major_formatter(formatter)
        self.fig_ax1.axis([error_range[0], error_range[1], error_yrange[0], error_yrange[1]])
        self.fig_ax1.add_artist(self.update_box)
        self.fig_ax1.add_line(self.error_line)
        self.fig_ax1.set_xlabel('Learning Steps', fontsize = 18)
        self.fig_ax1.set_ylabel('Error (MSPE) in log10', fontsize = 18)
        self.fig_ax1.set_clip_on(False)
        
        
        self.fig_ax2.set_xlim(inputs_range[0], inputs_range[1])
        self.fig_ax2.set_ylim(inputs_yrange[0], inputs_yrange[1])
        self.fig_ax2.add_line(self.results_line)
        self.fig_ax2.add_line(self.targets_line)
        self.fig_ax2.set_xlabel('Signal Input at Time T in Seconds', fontsize = 18)
        self.fig_ax2.set_ylabel(r'Signal Output Value', fontsize = 18)
        self.fig_ax2.set_yscale('linear')
        self.fig_ax2.axis([inputs_range[0], inputs_range[1], 
                          inputs_yrange[0], inputs_yrange[1]])
        self.fig_ax2.set_clip_on(False) 
        

        
        self.fig_ax1.grid(True) 
        self.fig_ax2.grid(True) 
        

        
    
    def init_animation(self, file_name):
        plt.ion()
        plt.show()
        self.fig1.suptitle(self.title, fontsize = 20)
        animation.TimedAnimation.__init__(self, self.fig1, blit=False, interval = 500, repeat = False)
        self.save(file_name)
        plt.clf();
        
        
    def set_targets(self, targets):
        if not self.scale_up is None:
            self.targets = [self.scale_up(i[0, 0]) for i in targets[:,1]]
        else:
            self.targets = [i[0, 0] for i in targets[:,1]]

    
    def init(self):
        self.results_line.set_data([],[])
        self.error_line.set_data([],[])
        return self.results_line, self.error_line
            
    def update_error( self, learning_step, error ):
        self.learning_steps.append(learning_step)
        self.errors.append(error)

    def update_results( self, inputs, y, learning_step ):
        #print inputs.shape
        #print y.shape
        # create the block matrix
        k = np.bmat([inputs, y])
        # sort the rows by the first column
        I = np.argsort(k[:,0], axis=0)
        # extract the first column from the sorted indices os the matrix
        # this is convoluted, but it helps preserve the structure of the 
        # existint matrix
        t  = k[I[:,0].A[:,0], :]
        #print t
        self.inputs.append([i[0, 0] for i in  t[:,0]])
        if not self.scale_up is None:
            self.results.append([self.scale_up(i[0, 0]) for i in t[:,1]])
        else:
            self.results.append([i[0, 0] for i in t[:,1]])
        
        self.update_error(learning_step, np.abs(self.mspe()))
        
        
    def mspe(self, idx = -1):
        y = np.array(self.results[-1])
        t = np.array(self.targets)
        err = np.sum(np.power(( t - y), 2)) - np.var(y) 
        #print err
        return err


    def _draw_frame(self, framedata):
        i = framedata
        
        if self.errors[i] < self.min_error:
            self.min_error = self.errors[i]
            self.step_min_error = self.learning_steps[i]
            self.update_box.set_text('Min Error, step: %d error: %0.03f'%(
                        self.step_min_error, self.min_error))
        
        print ("Drawing new frame %d, %d, %f"% (i, self.learning_steps[i], self.errors[i] ))
        
        self.error_line.set_data(self.learning_steps[:i],self.errors[:i])
        self.results_line.set_data(self.inputs[i],self.results[i])
        self.targets_line.set_data(self.inputs[i],self.targets)

        self._drawn_artists = [self.results_line, self.error_line]

    def new_frame_seq(self):
        return iter(range(len(self.learning_steps)))

    def _init_draw(self):
        lines =  [self.error_line, self.results_line]
        for l in lines:
            l.set_data([], [])
