'''
Created on Feb 25, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''


import matplotlib.pyplot as plt
from nnwork.ann.backprop import BPRecall




    
    
class PlotClass( object ):
    
    def __init__( self, epochs, k, max_x ):
        pass
    
    def update_error(self, epoch, error):
        pass
    
    def update_train(self, inputs, outputs):
        pass
    
    def update_test(self, inputs, outputs):
        pass
    
    
    