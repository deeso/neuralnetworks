'''
Created on Mar 28, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''
import scipy as sypy
import numpy as np


def get_error(X, Cx, W, Cw, Nns):
    error = 0;
    correct = 0;
    N = X.shape[1]
    for i in xrange(0, N):
        snorm = np.zeros ((Nns,1))
        snorm.shape = (Nns, )
        for j in xrange(0, Nns):
            snorm[j] = np.linalg.norm(X[:, i] -W[:,j]);
        idx = snorm.argmin(axis=0)
        if Cx[i] == Cw[idx]:
            correct += 1
        else:
            error += 1


    return float(correct)/N, float(error)/N


def LVQ1Learn(patterns, mu, inputs, class_cnt, Nns = 1, learning_steps = 2000, 
            scale = 1, origin = 0, monitor = 0, weights_method = None, wrange = [-.1, .1],
            assess_error = None, plotter = None ):
    '''
    Perform ANN online learning using the LVQ1 
    
    patterns: test patterns (e.g. inputs with the outputs) column vector format
    mu: learning rate of the network in 2x1 form (e.g. separate weights for 
            each layer)
    inputs: number of inputs in X, the rest are classes
    learning_steps (2000): number of steps to perform before quitting
    monitor (0): record the weights every this number of steps, 0 means dont 
        do that
    weights_method (uniform distribution): method to use when creating weights
    wrange ([-.1, .1]): range to use when creating the weight range
    
    assess_error(None): assessment function that takes the desired and output 
        from the cost function, and this can be used as another terminating 
        condition,
        e.g. 
           def assess_error(t, o):
              o[ o < 0] = -1
              o[ o >= 0] = 1
              return (t == o).all()


    
    returns a final W which are the derived Eigenvectors and weights 
        for the aformentioned elements 'monitor' number of steps, 
    plotter: ANN Plotter class used to track progress
    '''
    N = patterns.shape[1]
    Cx = patterns[inputs:, :]
    Cx.shape = (N,)
    
    
    WSize = (inputs, Nns)
    
    W = scale * sypy.random.uniform(wrange[0], wrange[1], size=WSize) - origin
    Cw = np.ceil(sypy.random.uniform(0, 1, size=(Nns,1))*class_cnt)
    Cw.shape = (Nns, )
    
    clearning_steps = 0
    
    if isinstance(mu, list):
        mu = mu[0]
    monitor_steps = {0:{"W":W}}
    correct, error = 0,0
    monitor_steps[clearning_steps]['error'] = error
    monitor_steps[clearning_steps]['correct'] = correct

    clearning_steps = 0
    last_dumped_performed = 0
    p = 0
    
    while clearning_steps < learning_steps:
        rate = mu
        if clearning_steps > 0:
            rate = mu #/ clearning_steps
        
    
        for i in xrange(0, N):
            snorm = np.zeros ((Nns,1))
            snorm.shape = (Nns, )
            for j in xrange(0, Nns):
                snorm[j] = np.linalg.norm(patterns[:inputs, i] -W[:,j]);
    
            idx = snorm.argmin(axis=0)
            if Cx[i] == Cw[idx]:
                W[:,idx] = W[:,idx] + rate * ( patterns[:inputs,i] - W[:,idx])
            else:
                W[:,idx] = W[:,idx] - rate * ( patterns[:inputs,i] - W[:,idx])
                         
        
        if monitor and (clearning_steps - last_dumped_performed) > monitor:
            print "Performed %d clearning_steps"%clearning_steps
            last_dumped_performed = clearning_steps
            monitor_steps[clearning_steps] = {"W":W.copy()}
            correct, error = get_error(patterns[:inputs, :], Cx, W, Cw, Nns)
            monitor_steps[clearning_steps]['error'] = error
            monitor_steps[clearning_steps]['correct'] = correct

            if not plotter is None:                
                plotter.update_results(W, clearning_steps)
            
            if not assess_error is None:
                if assess_error(W, W):
                    print ("assess_error: function successfully terminated at step %d"%clearning_steps)
                    break
        
        clearning_steps += 1
        
    monitor_steps[clearning_steps] = {"W":W.copy()}
    correct, error = get_error(patterns[:inputs, :], Cx, W, Cw, Nns)
    monitor_steps[clearning_steps]['error'] = error
    monitor_steps[clearning_steps]['correct'] = correct
    if not plotter is None:                
        plotter.update_results(W, clearning_steps)

    return W, Cw, monitor_steps

def LVQ1Recall(X, W, Cw, outputs ):
    '''
    Perform ANN recall with LVQ1 
    
    patterns: test patterns (e.g. inputs with the outputs) column vector format
    mu: learning rate of the network in 2x1 form (e.g. separate weights for 
            each layer)
    inputs: number of inputs in X, the rest are classes
    learning_steps (2000): number of steps to perform before quitting
    monitor (0): record the weights every this number of steps, 0 means dont 
        do that
    weights_method (uniform distribution): method to use when creating weights
    wrange ([-.1, .1]): range to use when creating the weight range
    
    assess_error(None): assessment function that takes the desired and output 
        from the cost function, and this can be used as another terminating 
        condition,
        e.g. 
           def assess_error(t, o):
              o[ o < 0] = -1
              o[ o >= 0] = 1
              return (t == o).all()


    
    returns a final W which are the derived Eigenvectors and weights 
        for the aformentioned elements 'monitor' number of steps, 
    plotter: ANN Plotter class used to track progress
    '''
    Nn = W.shape[1]
    N = X.shape[1]  
    Cxhat = np.zeros((outputs,N))
    Cxhat.shape = (Cxhat.size, )
    for i in xrange(0,N):
        d = np.zeros((1,Nn))
        d.shape = (d.size, )
        for j in xrange(0,Nn):
            d[j] = np.linalg.norm(W[:,j] - X[:,i]);
       
        idx = d.argmin(axis = 0)
        m = d[idx]
        Cxhat[i] = Cw[idx];
    
    return Cxhat