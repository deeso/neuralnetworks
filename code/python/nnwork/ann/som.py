'''
Created on Mar 15, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''

import scipy as sypy
import numpy as np
import math




required_for_convergence = 3
gone_without_change = 0
E_last = None
epsilon = 10e-20
def assess_error(t, o):
    global E_last, gone_without_change, required_for_convergence
    if E_last is None:
        E_last = o.copy()
        return False
    b = np.allclose(E_last, o)
    #b = E_last == o
    E_last = o.copy()
    if (b):
        gone_without_change += 1
        if gone_without_change == required_for_convergence/2:
            i = required_for_convergence/2
            print "Almost converged? at half the required iterations without change %d"%i
    else:
        gone_without_change = 0
    return gone_without_change >= required_for_convergence


def gaussian(shape, center, sigma):
    ''' 
    Function by: Nicolas P. Rougier 
    http://webloria.loria.fr/~rougier/coding/software/kohonen.py
    
    Return a two-dimensional gaussian with given shape.

    :Parameters:
       `shape` : (int,int)
           Shape of the output array
       `center`: (int,int)
           Center of Gaussian
       `sigma` : float
           Width of Gaussian
    '''
    grid=[]
    for size in shape:
        grid.append (slice(0,size))
    C = np.mgrid[tuple(grid)]
    R = np.zeros(shape)
    for i,size in enumerate(shape):
        R += ((C[i]-center[i])/float(shape[i]))**2
    return np.exp(-R/sigma**2)

def SOMLearn(patterns, init_alpha, som_shape, learning_steps = 2000, 
            init_sigma = .75, monitor = 0, weights_method = None, 
            wrange = [-.1, .1], assess_error = None, plotter = None, 
            final_alpha = .005, final_sigma = 0.010, neighborhood = gaussian):
    '''
    Perform SOM learning using the a neighborhood function 
    
    
    patterns: test patterns (e.g. inputs with the outputs)
    alpha: learning rate of the network in 2x1 form (e.g. separate weights for 
            each layer)
    som_shape: som dimensions (L, W, I) where L is length, W is width, I is inputs
    learning_steps (2000): number of steps to perform before quitting
         

    monitor (0): record the weights every this number of steps, 0 means dont 
        do that
    
    weights_method (uniform distribution): method to use when creating weights
    wrange ([-.1, .1]): range to use when creating the weight range
    
    assess_error(None): assessment function that takes the desired and output 
        from the cost function, and this can be used as another terminating 
        condition,
        e.g. 
           def assess_error(t, o):
              o[ o < 0] = -1
              o[ o >= 0] = 1
              return (t == o).all()


    
    returns a final W (input layer weights), V (hidden layer weights), and weights 
        for the aformentioned elements 'monitor' number of steps, 
    plotter: ANN Plotter class used to track progress
    
    Code from:  http://webloria.loria.fr/~rougier/coding/software/kohonen.py was used to 
    reference and debug this python implementation
    '''
    

    W = sypy.random.random(som_shape)
    t = 0
    monitor_steps = {0:{"W":W.copy(), 
                        "alpha":init_alpha, 
                        "sigma":init_sigma,
                        "time":t}}
    
    alpha = init_alpha
    sigma = init_sigma
    clearning_steps = 0
    last_dumped_performed = 0
    data = np.zeros((learning_steps, som_shape[-1]))
    while clearning_steps < learning_steps:
        t = clearning_steps/float(learning_steps)
        alpha = init_alpha + t*(final_alpha - init_alpha)
        sigma = init_sigma + t*(final_sigma - init_sigma)
        
        
        p = int(math.floor(np.random.randint(0, len(patterns))))
        data[clearning_steps] = patterns[ p, :]
        
        # calculate the distance matrix
        diff = W - data[clearning_steps]
        dist = ((diff)**2).sum(axis=-1)
        min_pos = np.unravel_index(np.argmin(dist), dist.shape)
        
        # calculate the cooperativeness of the SOM lattice 
        H_w =  neighborhood(dist.shape, min_pos, sigma)
        H_w = np.nan_to_num(H_w)
        
        for i in range(W.shape[-1]):
            W[..., i] -= alpha * H_w *  diff[..., i]
        
        
        if monitor and (clearning_steps - last_dumped_performed) > monitor:
            print "Performed %d clearning_steps"%clearning_steps
            last_dumped_performed = clearning_steps
            monitor_steps[clearning_steps] = {"W":W.copy(), 
                        "alpha":alpha, 
                        "sigma":sigma,
                        "time":t}

            if not plotter is None:                
                plotter.update_results(W, clearning_steps)
        
        clearning_steps += 1
        if not assess_error is None:
            if assess_error(None, W):
                print ("assess_error: function successfully terminated at step %d"%clearning_steps)
                break
        
    monitor_steps[clearning_steps] = {"W":W.copy(), 
                        "alpha":alpha, 
                        "sigma":sigma,
                        "time":t}
     
    
    if not plotter is None:                
        plotter.update_results(W, clearning_steps)

    return W, data, monitor_steps

