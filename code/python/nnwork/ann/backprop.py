'''
Created on Feb 22, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''

import scipy as sypy
import numpy as np
import math


def randomize_patterns(patterns, batch=2):
    '''
    batch: number of shuffled rows to return for the batched calculations
    patterns: row-wise inputs, where features and outputs are in the columns
    '''
    
    mat_shape = patterns.shape
    N = mat_shape[0]
    data = np.arange(N).reshape(-1, N).transpose() #@UndefinedVariable
    np.random.shuffle(data)
    patterns = patterns.copy()
    patterns = patterns[data[:, 0], :]
    return patterns[:batch, :]



def create_error_historys(error_function, weight_history, transfer, 
                        bias, slope, num_inputs, patterns = {}, scale_up=None):
    '''
    error_function: error function that takes 2 parameters, T, Y
    weight_history: history of weights
    transfer: transfer function for the network
    bias: bias used for the network
    num_inputs: number of inputs in the patterns
    patterns: dictionary by name for each of the patterns
    
    '''
    epochs = weight_history.keys()
    epochs.sort()
    
    names = patterns.keys()
    
    errors = {}
    
    for name in names:
        errors[name] = {}
        errors[name]['epoch'] = []
        errors[name]['error'] = []
        cpatterns = patterns[name]
        X = cpatterns[:, :num_inputs]
        T = cpatterns[:, num_inputs:]
        for epoch in epochs:
            W = weight_history[epoch]['W']
            V = weight_history[epoch]['V']
            Y = BPRecall(X, W, V, transfer, slope, bias)
            if not scale_up is None:
                Y = scale_up(Y) 
            e = error_function(T, Y)
            errors[name]['epoch'].append(epoch)
            errors[name]['error'].append(e)
    
    return errors

        
def BPComputeCosts(X, T, W, V, transfer, transfer_der, alpha, 
             momentum = None, error = None, error_der = None, 
             bias = [0,0], slope = 1 ):
    '''
       X: input matrix
       Y: desired outputs
       W: Input layer weights
       V: Hidden layer weights 
       tranfer_der: must be a vectorized function def for activation
       tranfer: must be a vectorized function def for activation
       alpha: input and hidden layer learning rates (2 x 1 Matrix)
       momentum (optional): momentum values for layers
       error  (optional): error function
       error_der  (optional): error derivative
       bias ([0,0]):  bias elements 
    '''
    
    if error is None:
        error = np.vectorize(lambda t, o: .5 * np.power((o - t), 2))
        error_der = np.vectorize(lambda t, o: (o - t))
    
    if bias[0]:
        bias_col = np.ones((X.shape[0], 1))
        X = np.bmat([bias_col, X])        

    
    # inputs multiplied by the weights
    NET_h = X * W
    Y_h = transfer(slope, NET_h)
    
    # if there is a bias handle that condition
    if bias[1]:
        bias_col = np.ones((NET_h.shape[0], 1))
        Y_h = np.bmat([bias_col, Y_h])        
        
    
    # hidden layer inputs multiplied by the weights
    #NET = Y_h * V.transpose()
    #print Y_h
    #print V
    NET = Y_h * V
    # calculate output
    Y = transfer(slope, NET)
    
    
    delta = error_der(Y, T)
    delta = np.multiply(delta, transfer_der(slope, NET)) 
    
    v = V.transpose()
    if bias[1]:
        v = v[:, 1:]
    
    delta_h = delta * v
    delta_h  = np.multiply(delta_h, transfer_der(slope, NET_h))    
    
    
    
    
    J_v = alpha[1] * delta.transpose() * Y_h
    J_w = alpha[0] * delta_h.transpose() * X
    E = error(Y, T)
    
    
    return J_w.transpose(), J_v.transpose(), E, Y




def BPLearnOnline(patterns, alpha, inputs, outputs, hidden, 
        learning_steps = 2000, momentum = [0, 0], transfer = None, 
        transfer_der = None, 
        error = None, error_der = None, monitor = 0, slope = 1,
        weights_method = None, wrange = [-.1, .1], bias = [0, 0],
        random = 1, assess_error = None, plotter = None):
    '''
    Perform ANN online learning using the BackPropogation Algorithm
    
    
    patterns: test patterns (e.g. inputs with the outputs)
    alpha: learning rate of the network in 2x1 form (e.g. separate weights for
             each layer)
    inputs: number of inputs
    outputs: number of outputs
    hidden: number of hidden elements
    learning_steps (2000): number of steps to perform before quitting
    momentum ([0, 0]: 2x1 list indicating how much to multiply the delta of 
            the weight to help speed the learning along (e.g. nudge out of a 
            local minima)
    transfer (None): transfer or activation function to apply during learning, 
         default is tanh,  Also it should be noted that this needs to be a 
         numpy.vectorized function
    transfer_der (None): differential of the transfer or activation function to apply 
            during learning, default is tanh,
         Also it should be noted that this needs to be a numpy.vectorized function
    
    *** Note that transfer and transfer_der must take TWO paramters, one is the 
        output of the network, and the other is the slope parameter, SLOPE DOES NOT 
        need to be used. e.g.
    
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))
     
     where slope and o are slope and output respectively
    
    epsilon (.0005): termination condition of the network (e.g. difference in error)
    error (MSE): error function to use during backprop error calculation
    error_der (MSE): differential of the error function to use during backprop 
        error calculation

    *** Note that error and error_der must take TWO paramters, one is the output of the
    network, and the other is the target value e.g. 
            
            error = np.vectorize(lambda t, o: .5 * np.power((o - t), 2))

    where t and o are target and output respectively
         

    monitor (0): record the weights every this number of steps, 0 means dont do that
    slope (1): slope to use in the tanh  
    
    weights_method (uniform distribution): method to use when creating weights
    wrange ([-.1, .1]): range to use when creating the weight range
    bias ([0, 0]): 2x1 matrix indicating with 1 to use a bias in the input layer and
         hidden layers of the network respectively
    
    assess_error(None): assessment function that takes the desired and output 
            from the cost function, and this can be used as another terminating 
            condition e.g. 
           def assess_error(t, o):
              o[ o < 0] = -1
              o[ o >= 0] = 1
              return (t == o).all()

    returns a final W (input layer weights), V (hidden layer weights), and weights 
            for the aformentioned elements 'monitor' number of steps, 
    '''

    
    
    
    # create sizes for the weight matices
    WSize = (inputs+bias[0], hidden - bias[1])
    VSize = (hidden, outputs)
    
    # create for the weight matices from the weight ranges    
    W = sypy.random.uniform(wrange[0], wrange[1], size=WSize)
    V = sypy.random.uniform(wrange[0], wrange[1], size=VSize)
    monitor_steps = {0:{"W":W, "V":V}}
    # known initial values to test network convergence
    #W = np.matrix([[-0.04, -0.32, 0.05],[0.07, 0.40, 0.31]]).transpose()
    #V = np.matrix([ -0.03, -0.14, 0.47]).transpose()

    
    if transfer is None:
        # vectorize tanh, and then the activation functions
        # which are disguised as lambdas
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))
        transfer_der = np.vectorize(lambda slope, o: slope * (1 - 
                                                    tanh(slope * o) * tanh(slope * o)))
    
    dW = np.zeros(W.shape)
    dV = np.zeros(V.shape)
    
    clearning_steps = 0
    last_dumped_performed = 0
    while clearning_steps < learning_steps:
        p = clearning_steps%len(patterns)
        if random:
            p = int(math.floor(np.random.randint(0, len(patterns))))
        
        
        X = patterns[p, :inputs]
                    
        T = patterns[p, inputs:]
        
        J_w, J_v, E, Y = BPComputeCosts(X, T, W, V, transfer, transfer_der, alpha, 
                                         momentum = momentum, error = error, 
                                         error_der = error_der, 
                                         bias = bias, slope = slope)
        #print "J_w = \n", J_w.transpose()
        #print "J_v = \n", J_v.transpose()
        #print "E = \n", E
        dW = J_w - dW * momentum[0]
        dV = J_v - dV * momentum[1]
        W = W + dW
        V = V + dV
        
        if monitor and (clearning_steps - last_dumped_performed) > monitor:
            print "Performed %d clearning_steps"%clearning_steps
            last_dumped_performed = clearning_steps
            monitor_steps[clearning_steps] = {}
            monitor_steps[clearning_steps]["W"] = W.copy()
            monitor_steps[clearning_steps]["V"] = V.copy()

            if not plotter is None:                
                plotter.update_results(patterns[:, :inputs], BPRecall(patterns[:, :inputs], 
                        W, V, transfer, slope, bias), clearning_steps)
        
        clearning_steps += 1

        if not assess_error is None:
            if assess_error(patterns[:, inputs:], 
                       BPRecall(patterns[:, :inputs], 
                        W, V, transfer, slope, bias)):
                print ("assess_error: function successfully terminated")
                break
            
        
    
    monitor_steps[clearning_steps] = {}
    monitor_steps[clearning_steps]["W"] = W.copy()
    monitor_steps[clearning_steps]["V"] = V.copy()

    print ("Completed in %d learning steps"%clearning_steps)

    if not plotter is None:                
        plotter.update_results(patterns[:, :inputs], BPRecall(patterns[:, :inputs], 
                W, V, transfer, slope, bias), clearning_steps)

    return W, V, monitor_steps
        
        
        


def BPLearn(patterns, alpha, inputs, outputs, hidden, learning_steps = 2000, 
            epoch = 1, momentum = [0, 0], transfer = None, transfer_der = None,
            error = None, error_der = None, monitor = 0, testPatterns = None, 
            slope = 1, weights_method = None, bias = [0, 0], wrange = [-.1, .1],
            random=1, assess_error = None, plotter = None ):
    '''
    Perform ANN batch learning using the BackPropogation Algorithm
    
    
    patterns: test patterns (e.g. inputs with the outputs)
    alpha: learning rate of the network in 2x1 form (e.g. separate weights for 
            each layer)
    inputs: number of inputs
    outputs: number of outputs
    hidden: number of hidden elements
    learning_steps (2000): number of steps to perform before quitting
    epoch (1): number of steps to perform before updating the weights. if epoch
        is one, this is online/stochastic training and epoch > 1 is batch 
        training
    momentum ([0, 0]: 2x1 list indicating how much to multiply the delta of the 
        weight to help speed the learning along (e.g. nudge out of a local 
        minima)
    transfer (None): transfer or activation function to apply during learning, 
        default is tanh, Also it should be noted that this needs to be a 
        numpy.vectorized function
    transfer_der (None): differential of the transfer or activation function to apply 
            during learning, default is tanh,
         Also it should be noted that this needs to be a numpy.vectorized function
    
    *** Note that transfer and transfer_der must take TWO paramters, one is the 
        output of the network, and the other is the slope parameter, SLOPE DOES 
        NOT need to be used. e.g.
    
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))
     
     where slope and o are slope and output respectively
    
    error (MSE): error function to use during backprop error calculation
    error_der (MSE): differential of the error function to use during backprop 
            error calculation

    *** Note that error and error_der must take TWO paramters, one is the 
    output of the network, and the other is the target value e.g. 
            
            error = np.vectorize(lambda t, o: .5 * np.power((o - t), 2))

    where t and o are target and output respectively
         

    monitor (0): record the weights every this number of steps, 0 means dont 
        do that
    slope (1): slope to use in the tanh  
    
    weights_method (uniform distribution): method to use when creating weights
    wrange ([-.1, .1]): range to use when creating the weight range
    bias ([0, 0]): 2x1 matrix indicating with 1 to use a bias in the input 
        layer and
         hidden layers of the network respectively
    
    assess_error(None): assessment function that takes the desired and output 
        from the cost function, and this can be used as another terminating 
        condition,
        e.g. 
           def assess_error(t, o):
              o[ o < 0] = -1
              o[ o >= 0] = 1
              return (t == o).all()


    
    returns a final W (input layer weights), V (hidden layer weights), and weights 
        for the aformentioned elements 'monitor' number of steps, 
    '''
    

    if epoch == 1:
        # perform online learning
        return BPLearnOnline(patterns, alpha, inputs, outputs, hidden, 
                         learning_steps=learning_steps, 
                         momentum=momentum, transfer=transfer, 
                         transfer_der=transfer_der, error=error, 
                         error_der=error_der, monitor=monitor, slope=slope, 
                         weights_method=weights_method, bias=bias, wrange=wrange, 
                         random=random, assess_error = assess_error, 
                         plotter = plotter)

    

    # create sizes for the weight matices
    WSize = (inputs+bias[0], hidden - bias[1])
    VSize = (hidden, outputs)
    
    # create for the weight matices from the weight ranges    
    W = sypy.random.uniform(wrange[0], wrange[1], size=WSize)
    V = sypy.random.uniform(wrange[0], wrange[1], size=VSize)
    monitor_steps = {0:{"W":W, "V":V}}
    if transfer is None:
        # vectorize tanh, and then the activation functions
        # which are disguised as lambdas
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))
        transfer_der = np.vectorize(lambda slope, o: slope * (1 - 
                                        tanh(slope * o) * tanh(slope * o)))
    
    dW = np.zeros(W.shape)
    dV = np.zeros(V.shape)
    
    clearning_steps = 0
    last_dumped_performed = 0
    epochpatterns = patterns#randomize_patterns(patterns, epoch)
    while clearning_steps < learning_steps:
        # lazy load using the first epoch of patterns
        X_batch = epochpatterns[:, :inputs]
        T_batch = epochpatterns[:, inputs:]
        epochpatterns = randomize_patterns(patterns, epoch)
        #if bias[0]:
        #    bias_col = np.ones((X_batch.shape[0], 1))
        #    X_batch = np.bmat([bias_col, X_batch])
            
        
        J_w, J_v, E, Y = BPComputeCosts(X_batch, T_batch, W, V, transfer, 
                        transfer_der, alpha, momentum = momentum, 
                        error = error, error_der = error_der, bias = bias, 
                        slope = slope)
        #print dW
        #print dV
        dW = J_w - dW * momentum[0]
        dV = J_v - dV * momentum[1]
        W = W + dW
        V = V + dV
        
        
        if monitor and (clearning_steps - last_dumped_performed) > monitor:
            print "Performed %d clearning_steps"%clearning_steps
            last_dumped_performed = clearning_steps
            monitor_steps[clearning_steps] = {}
            monitor_steps[clearning_steps]["W"] = W.copy()
            monitor_steps[clearning_steps]["V"] = V.copy()

            if not plotter is None:                
                plotter.update_results(patterns[:, :inputs], BPRecall(patterns[:, :inputs], 
                        W, V, transfer, slope, bias), clearning_steps)
        
        clearning_steps += len(epochpatterns)
        if not assess_error is None:
            if assess_error(patterns[:, inputs:], 
                       BPRecall(patterns[:, :inputs], 
                        W, V, transfer, slope, bias)):
                print ("assess_error: function successfully terminated")
                break
            
        

        
        
    monitor_steps[clearning_steps] = {}
    monitor_steps[clearning_steps]["W"] = W
    monitor_steps[clearning_steps]["V"] = V
    if not plotter is None:                
        plotter.update_results(patterns[:, :inputs], BPRecall(patterns[:, :inputs], 
                W, V, transfer, slope, bias), clearning_steps)

    return W, V, monitor_steps




def BPRecall(X, W, V, transfer = None, slope = 1, bias = [0, 0]):
    '''
    Perform recall using the weights created for the neural network.  If a 
    history is provided, this function will also 
    
    X: input pattern
    W: input layer weights
    V: hidden layer weights
    transfer (None): transfer or activation function to apply during learning, 
        default is tanh, Also it should be noted that this needs to be a 
        numpy.vectorized function
    
    *** Note that transfer must take TWO paramters, one is the output of the
    network, and the other is the slope parameter, SLOPE DOES NOT need to be 
        used. e.g.
    
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))
     
     where slope and o are slope and output respectively
     
     bias([0,0]): if bias should be applied to the network then the 1 is added
     
     return back the Y output value

    '''
    
    if transfer is None:
        # vectorize tanh, and then the activation functions
        # which are disguised as lambdas
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))

    if bias[0]:
        bias_col = np.ones((X.shape[0], 1))
        X = np.bmat([bias_col, X])        
        
    # inputs multiplied by the weights
    NET_h = X * W
    Y_h = transfer(slope, NET_h)
    
    # if there is a bias handle that condition
    if bias[1]:
        bias_col = np.ones((NET_h.shape[0], 1))
        Y_h = np.bmat([bias_col, Y_h])        
        
    
    # hidden layer inputs multiplied by the weights
    #NET = Y_h * V.transpose()
    #print Y_h
    #print V
    NET = Y_h * V
    # calculate output
    Y = transfer(slope, NET)
    
    return Y
    
    
    
    
    

def BPRecallHistory(X, T, recorded_history = {}, bias = [0, 0],
                    transfer = None, slope = 1, error = None):
    '''
    Perform recall on the given history, and s 
    
    X: input pattern
    W: input layer weights
    V: hidden layer weights
    transfer (None): transfer or activation function to apply during 
        learning, default is tanh, Also it should be noted that this needs 
        to be a numpy.vectorized function
    
    *** Note that transfer must take TWO paramters, one is the output of the
    network, and the other is the slope parameter, SLOPE DOES NOT need to be 
        used. e.g.
    
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))
     
     where slope and o are slope and output respectively
    
    error (MSE): error function to use during backprop error calculation
    
    *** Note that error and error_der must take TWO paramters, one is the 
        output of the network, and the other is the target value e.g. 
            
            error = np.vectorize(lambda t, o: .5 * np.power((o - t), 2))

    return back the error_history and the output_history
    
    '''
    output_history = {}
    error_history = {}
    
    if transfer is None:
        # vectorize tanh, and then the activation functions
        # which are disguised as lambdas
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))

    if error is None:
        error = np.vectorize(lambda t, o: .5 * np.power((o - t), 2))
    
    historys = recorded_history.keys()
    
    for history in historys:
        W = recorded_history[history]['W']
        V = recorded_history[history]['V']
        Y = BPRecall(X, W, V, transfer, slope, bias)
        error_history[history] = error(T, Y)
        output_history[history] = Y
        
    return output_history, error_history
    
    
    
