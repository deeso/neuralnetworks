'''
princomp.py: Source 
http://glowingpython.blogspot.com/2011/07/principal-component-analysis-with-numpy.html

Accessed: March 9, 2012

Author: Unknown
'''
import numpy as np

def princomp(A):
    """ performs principal components analysis 
        (PCA) on the n-by-p data matrix A
        Rows of A correspond to observations, columns to variables. 
    
    Returns :  
     coeff :
       is a p-by-p matrix, each column containing coefficients 
       for one principal component.
     score : 
       the principal component scores; that is, the representation 
       of A in the principal component space. Rows of SCORE 
       correspond to observations, columns to components.
    
     latent : 
       a vector containing the eigenvalues 
       of the covariance matrix of A.
    """
    # computing eigenvalues and eigenvectors of covariance matrix
    mu = np.tile(np.mean(A.T,axis=1).T, (A.shape[0], 1))
    M = (A.copy()-mu).T # subtract the mean (along columns)
    
    [latent,coeff] = np.linalg.eig(np.cov(M))
    score = np.dot(coeff.T,M) # projection of the data in the new space
    coeff = np.matrix(coeff)
    return coeff,score,latent




def PCLearn(patterns, alpha, inputs, outputs,  plotter = None ):
    '''
    Perform ANN batch learning using PCs 
    
    
    patterns: test patterns (e.g. inputs with the outputs)
    alpha: learning rate of the network in 2x1 form (e.g. separate weights for 
            each layer)
    inputs: number of inputs
    outputs: number of outputs
    hidden: number of hidden elements
    learning_steps (2000): number of steps to perform before quitting
    epoch (1): number of steps to perform before updating the weights. if epoch
        is one, this is online/stochastic training and epoch > 1 is batch 
        training
    momentum ([0, 0]: 2x1 list indicating how much to multiply the delta of the 
        weight to help speed the learning along (e.g. nudge out of a local 
        minima)
    transfer (None): transfer or activation function to apply during learning, 
        default is tanh, Also it should be noted that this needs to be a 
        numpy.vectorized function
    transfer_der (None): differential of the transfer or activation function to apply 
            during learning, default is tanh,
         Also it should be noted that this needs to be a numpy.vectorized function
    
    *** Note that transfer and transfer_der must take TWO paramters, one is the 
        output of the network, and the other is the slope parameter, SLOPE DOES 
        NOT need to be used. e.g.
    
        tanh = np.vectorize(math.tanh)
        transfer = np.vectorize(lambda slope, o: tanh(slope * o))
     
     where slope and o are slope and output respectively
    
    error (MSE): error function to use during backprop error calculation
    error_der (MSE): differential of the error function to use during backprop 
            error calculation

    *** Note that error and error_der must take TWO paramters, one is the 
    output of the network, and the other is the target value e.g. 
            
            error = np.vectorize(lambda t, o: .5 * np.power((o - t), 2))

    where t and o are target and output respectively
         

    monitor (0): record the weights every this number of steps, 0 means dont 
        do that
    slope (1): slope to use in the tanh  
    
    weights_method (uniform distribution): method to use when creating weights
    wrange ([-.1, .1]): range to use when creating the weight range
    bias ([0, 0]): 2x1 matrix indicating with 1 to use a bias in the input 
        layer and
         hidden layers of the network respectively
    
    assess_error(None): assessment function that takes the desired and output 
        from the cost function, and this can be used as another terminating 
        condition,
        e.g. 
           def assess_error(t, o):
              o[ o < 0] = -1
              o[ o >= 0] = 1
              return (t == o).all()


    
    returns a final W (input layer weights), V (hidden layer weights), and weights 
        for the aformentioned elements 'monitor' number of steps, 
    '''
    

    W,z,w = princomp(patterns.copy()[:, :inputs]) 
    monitor_steps = {0:{'W':W}}
    return W, monitor_steps




def PCRecall(X, W, transfer = None, slope = 1):
    '''
    Perform recall using the weights created for the neural network.  If a 
    history is provided, this function will also 
    
    X: input pattern
    W: input layer weights
     
     return back the Y output value

    '''
    

    return X * W