'''
Created on Mar 9, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''
import scipy as sypy
import numpy as np
import math

def center_inputs(patterns, inputs):
    A = patterns[:, :inputs].copy()
    mu = np.tile(np.mean(A.T,axis=1).T, (A.shape[0], 1))
    M = (A-mu) # subtract the mean (along columns)
    patterns = patterns.copy()
    patterns[:, :inputs] = M
    return patterns


def randomize_patterns(patterns, batch=2):
    '''
    batch: number of shuffled rows to return for the batched calculations
    patterns: row-wise inputs, where features and outputs are in the columns
    '''
    mat_shape = patterns.shape
    N = mat_shape[0]
    data = np.arange(N).reshape(-1, N).transpose()
    np.random.shuffle(data)
    patterns = patterns.copy()
    patterns = patterns[data[:, 0], :]
    return patterns[:batch, :]


def GHALearn(patterns, alpha, inputs, pcs=None, learning_steps = 2000, 
            monitor = 0, weights_method = None, wrange = [-.1, .1],
            assess_error = None, plotter = None ):
    '''
    Perform ANN online learning using the GHA 
    
    patterns: test patterns (e.g. inputs with the outputs)
    alpha: learning rate of the network in 2x1 form (e.g. separate weights for 
            each layer)
    inputs: number of inputs
    pc: number of PCs to learn
    learning_steps (2000): number of steps to perform before quitting
    monitor (0): record the weights every this number of steps, 0 means dont 
        do that
    weights_method (uniform distribution): method to use when creating weights
    wrange ([-.1, .1]): range to use when creating the weight range
    
    assess_error(None): assessment function that takes the desired and output 
        from the cost function, and this can be used as another terminating 
        condition,
        e.g. 
           def assess_error(t, o):
              o[ o < 0] = -1
              o[ o >= 0] = 1
              return (t == o).all()


    
    returns a final W which are the derived Eigenvectors§ and weights 
        for the aformentioned elements 'monitor' number of steps, 
    plotter: ANN Plotter class used to track progress
    '''
    if pcs is None:
        pcs = inputs
    
    if isinstance(alpha, list):
        alpha = alpha[0]
    
    # create sizes for the weight matices
    WSize = (pcs, inputs)
    
    # create for the weight matices from the weight ranges    
    W = sypy.random.uniform(wrange[0], wrange[1], size=WSize)
    
    #print "Initial Weights:"
    #print W, '\n'
    monitor_steps = {0:{"W":W}}
    
    dW = np.zeros(W.shape)
    
    clearning_steps = 0
    last_dumped_performed = 0
    p = 0
    
    while clearning_steps < learning_steps:
        X_batch = patterns[:, :inputs]
        Y =  W * X_batch.T
        J_w = alpha * (Y *  X_batch - np.tril( Y * Y.T )* W)
        W = W + J_w + alpha * dW
        dW = J_w
        
        if monitor and (clearning_steps - last_dumped_performed) > monitor:
            print "Performed %d clearning_steps"%clearning_steps
            last_dumped_performed = clearning_steps
            monitor_steps[clearning_steps] = {"W":W.copy()}

            if not plotter is None:                
                plotter.update_results(W, clearning_steps)
        
        clearning_steps += len(X_batch)
        if not assess_error is None:
            if assess_error(np.eye(W.shape[0], W.shape[1]), W):
                print ("assess_error: function successfully terminated at step %d"%clearning_steps)
                break
        
    monitor_steps[clearning_steps] = {"W":W.copy()}
    if not plotter is None:                
        plotter.update_results(W, clearning_steps)

    return W.T, monitor_steps

def GHAGetComponents(X, W):
    '''
    get the components based on the derived eigenvectors  
    X: input pattern
    W: Eigenvectors derived from GHA
    return back the components from the Eigenvectors (W)
    '''
    return np.dot(X, W)

def GHARecall(PCs, W, mu):
    '''
    Perform recall using the eigenvectors derived from GHA.  
    
    PCs: principal components
    W: Eigenvectors derived from GHA
    mu: mean from the original input data, X
     
    return the recalled X based on the PCs, Eigenvectors, and the mean

    '''
    return np.dot(W,PCs).T + mu
