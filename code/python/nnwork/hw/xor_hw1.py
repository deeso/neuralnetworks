'''
Created on Feb 24, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''
import math
import numpy as np
from nnwork.ann.backprop import BPLearn, BPRecall, BPRecallHistory,\
    create_error_historys
from matplotlib import pyplot as plt
import time
from nnwork.annplots.plotterdef import PlotClass


class XorPlotter( PlotClass ):

    def __init__( self, epochs, k, max_x ):
        PlotClass.__init__(self, epochs, k, max_x)
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot( 211 )
        self.ax1.set_ylim(0,10)
        self.ax1.set_xlim(0,epochs+10)
        self.ax1.set_xlabel('Learning Steps in Epochs, Epoch = %d learning steps'%k)
        
        self.ax2 = self.fig.add_subplot( 212 )
        self.ax2.set_xlabel('Training Output')
        self.epochs = []
        self.errors = []
        
        self.labels = ['00', '01', '10', '11']
        self.labels = [0, 1, 2, 3]
        self.train_inputs = {}
        self.train_outputs = {}
        
        self.test_inputs = []
        self.test_outputs = []
        for i in self.labels:
            self.train_inputs[i] = []
            self.train_outputs[i] = []
            
    def update_error( self, epoch, error ):
        self.epochs.append(epoch)
        self.errors.append(error)
        self.ax1.plot(self.epochs, self.errors, 'b-')
        #self.ax2.plot(epochs, errors, 'b-')
        plt.draw()
        time.sleep(1)

    def update_train( self, inputs, outputs ):
        i = 0
        x = []
        y = []
        while i < 4:
            self.train_inputs[i].append(inputs[i])
            x.append(self.epochs[-1])
            self.train_outputs[i].append(outputs.ravel()[0,i])
            y.append(self.train_outputs[i][0])
            i += 1
        
        
        self.ax2.plot(x[0],y[0],'or-', 
                      x[1],y[1],'ob-',
                      x[2],y[2],'og-',
                      x[3],y[3],'ok-', )
        plt.draw()
        time.sleep(1)
    


def error_t(t, o):
    return np.mean(.5 * np.power((o - t), 2))

def rms(e):
    e = np.mean(e)
    return np.power(e, .5)


def rms_t(t, o):
    return rms(t - o)



def assess_error(t, o):
    p = post_process_output(o.copy())
    #print o
    #print p
    return (t == p).all()

def post_process_output(o):
    o[ o >= 0] = 1
    o[ o < 0] = -1
    return o

def process_output(o):
    o[ o >= 0] = 1
    o[ o < 0] = 0
    return o



# activation criteria
slope = 1
tanh = np.vectorize(math.tanh)
transfer = np.vectorize(lambda slope, o: tanh(slope * o))
transfer_der = np.vectorize(lambda slope, o: slope * (1 - 
                                            tanh(slope * o) * tanh(slope * o)))

error = None
error_der = None

# network configuration
inputs = 1
outputs = 1
hidden = 80
bias = [1, 1]
wrange = [-.5, .5]
weights_method = None

# learning parameters
alpha = [.015, .015];
momentum = [0, 0];
epsilon = .05;
learning_steps = 6000;
monitor = 100;
epoch = 1

plotter = XorPlotter(learning_steps/epoch+1, epoch, learning_steps/epoch+1)

# Known Final Values
W_f = np.matrix([[ 1.36577625, -0.75200461],
 [-1.04194014,  2.22766534],
 [-1.02425208,  2.16026472]])

V_f = np.matrix([[-1.51218265],
 [ 2.10001688],
 [ 2.00265021]])


# Known Initial Values

patterns = np.matrix( [[0, 0, -1], [0, 1, 1], [1, 0, 1],[1, 1, -1]])

# trainpatterns need to be either {-1, 1}
trainingPatterns = patterns


W, V, history = BPLearn(trainingPatterns, alpha, inputs, outputs, hidden, 
                         learning_steps=learning_steps, epoch=epoch, 
                         momentum=momentum, transfer=transfer, 
                         transfer_der=transfer_der, error=error, 
                         error_der=error_der, monitor=monitor, slope=slope, 
                         weights_method=weights_method, bias=bias, wrange=wrange, 
                         epsilon=epsilon, assess_error = assess_error, random = 1,
                         plotter = plotter)

Y = BPRecall(trainingPatterns[:,:inputs], W, V, transfer = transfer, 
            slope = slope, bias = bias)

output_history, error_history = BPRecallHistory(trainingPatterns[:,:inputs], 
                trainingPatterns[:,inputs:], history, bias, transfer, slope, error)


ehistory = create_error_historys(error_t, history, transfer, bias, slope, inputs, {'training':patterns})
epochs = history.keys()
epochs.sort()
errors = [history[k]['Error'] for k in epochs]

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(0,max(errors))
ax.set_xlim(0,max(epochs))
ax.set_xlabel('Learning Steps in Epochs, Epoch = %d'%epoch)
ax.plot(epochs[0:], errors[0:], 'ob-')
ax.set_ylabel('s1 and s2')
ax.grid(True)
#ax.draw()
plt.show()