'''
Created on Mar 28, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''
import numpy as np
import scipy as sypy
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import math
import random
from nnwork.ann.lvq1 import LVQ1Learn, LVQ1Recall

plt.ion()
def plot_prediction(steps, error_1, error_2):
    '''
    plot all three output errors on the same figure with three
    different subplots.  then create plots with the correct
    formula describing the output formula
    
    X: inputs
    Y_train: training  outputs
    Y_test: test  outputs
    Y_test2: second set of test outputs 
    
    '''

    # plot the results from the prediction
    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    
    
    title = '''Classification Error'''
    
    #fig.legend(("Training", "Test 1", "Test 2"),
    #           loc = 0, shadow=True, fancybox=True)
    
    plt.xlabel('Learning Steps',  fontsize=16)
    
    fig.suptitle(title, fontsize = 16)
    
    
    #ax = fig.add_subplot(311)
    ax.set_ylabel('Classification Error',  fontsize=16)
    
    ax.grid(True)
    ax.set_ylabel('Error',  fontsize=16)
    ax.set_xlim(0, max(steps))
    #ax1 = fig.add_subplot(312)
    k = plt.Line2D(steps, error_1, label=r'Training 1: 9x9 Matrix of Classes', 
                   color="blue",   marker='o', lw = 3, ls=":")
    l = plt.Line2D(steps, error_2, label=r'Training 1: 27x27 Matrix of Classes', 
                   color="red",   marker='o', lw = 3, ls="--")
    ax.add_line(k)
    ax.add_line(l)
    ax.legend(loc='upper left')
    ax.grid(True)
    
    #plt.legend([ax, ax1, ax2], ["Training", "Test Set 1", "Test Set 2"], loc=(1.1,0.1)) 
    plt.show()


A = np.mat([[3, 1, 1, 1, 1, 2, 3, 3, 3],
     [3, 3, 3, 3, 3, 2, 3, 3, 3],
     [3, 3, 3, 3, 3, 1, 2, 2, 2],
     [3, 1, 1, 1, 1, 1, 1, 1, 1],
     [3, 1, 1, 1, 2, 1, 1, 1, 1],
     [3, 1, 1, 2, 2, 2, 1, 1, 1],
     [3, 1, 2, 2, 1, 3, 2, 2, 2],
     [3, 3, 3, 3, 3, 3, 2, 2, 2],
     [3, 3, 3, 3, 3, 3, 3, 3, 2]])


def extrapolate_matrix(A, sz = 3):
    p = []
    for i in xrange(A.shape[0]):
        row = [A[i, j] * np.ones((sz,sz)) for j in xrange(A.shape[1])]
        p.append(np.bmat(row))
            
    q = p[0]
    for i in xrange(1, len(p)):
        q = np.bmat([[q], [p[i]]])
        
    return q


def run_once(p = A, class_cnt = 3, Nns = 60, learning_steps = 1000, scale = 8, 
             origin_shift = 4, xlims = [-4, 4], ylims = [-4, 4]):
    Cx = p.copy()
    Cx.shape = (1, p.size)
    patterns = np.zeros((3, p.size))
    
    
    k = 0;
    for i in xrange(0, p.shape[1]):
        for j in xrange(0, p.shape[0]):
            patterns[0,k] = i-4;
            patterns[1,k] = j-4;
            patterns[2,k] = p[i, p.shape[0]-j-1]
            k += 1
    
    inputs = 2
    mu = 0.01
    
    monitor = learning_steps/100
    weights_method = None
    
    wrange = [-.1, .1]
    wrange = [0, 1]
    assess_error = None
    plotter = None
    Nn = Nns;
    outputs = 1
    
    W, Cw, history = LVQ1Learn(patterns, mu, inputs, class_cnt, Nns = Nn, scale = scale,
                           origin = origin_shift, learning_steps=learning_steps, 
                           monitor=monitor, weights_method=None, wrange=wrange, 
                           assess_error=assess_error, plotter = plotter)
    
    Cxhat = LVQ1Recall(patterns[:inputs, :], W, Cw, outputs)
    
    
    Cxhat.shape = p.shape
    Cxhat.shape = (Cx.size,)
    out_image = np.zeros(p.shape)
    
    for row in xrange(0, p.shape[0]):
        for col in xrange(0, p.shape[1]):
            try:
                out_image[row, col] = Cxhat[p.shape[0]*(row) + col];
            except:
                raise

    fig, ax = plt.subplots(nrows=1, sharex=True, figsize=(12,8))
    fig.suptitle("Classification Regions after %d Learning Steps"%(learning_steps), fontsize = 16)
    
    #ax.legend(["Class 1", "Class 2", "Class 3"], loc=(1.1,0.1))
    ax.imshow(out_image, extent = (xlims + ylims), cmap=cm.binary, interpolation='nearest')
    plt.show()
    return W, Cw, history, Cxhat


ylims = [-4, 4]
xlims = [-4, 4]
r = hw8_lvq1.run_once(p = hw8_lvq1.A, class_cnt = 3, Nns = 60, 
            learning_steps = 1000, scale = 8, origin_shift = 4, 
            xlims = xlims, ylims = ylims)

h = r[2]
steps = h.keys()
steps.sort()
error = [h[i]['error'] for i in steps if i in h]





q  =hw8_lvq1.extrapolate_matrix(hw8_lvq1.A)
ylims = [-q.shape[0], q.shape[0]]
xlims = [-q.shape[1], q.shape[1]]
r2 = hw8_lvq1.run_once(p = q, class_cnt = 3, Nns = 60, 
                learning_steps = 1000, scale = sum(q.shape), origin_shift = q.shape[0], 
                xlims = xlims, ylims = ylims)

h2 = r2[2]
steps = h2.keys()
steps.sort()
error = [h2[i]['error'] for i in steps]
