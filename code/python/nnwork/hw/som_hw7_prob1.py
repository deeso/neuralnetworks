'''
Created on Mar 9, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''

import matplotlib
from nnwork.hw.patterns import patterns
from nnwork.ann.som import SOMLearn, assess_error
matplotlib.use('Qt4Agg')
from matplotlib import pyplot as plt
import numpy as np

import numpy as np
import scipy

def create_plot(W, data, title):
    
    som_space = {}
    #for i in W.shape[0]:
    #    som_space[i] = {}
    #    for j in W.shape[1]:
    #        som_space[i][j] = {'X1':0, 'X2':0, 'X3':0, 'X4':0}
    
    
    fig = plt.figure(figsize=(12,12))
    fig.suptitle(title, fontsize = 20)
    ax = fig.add_subplot(211)
    
    
    ax.axis([0,1,0,1])
    #ax.set_xticks([])
    #ax.set_yticks([])
    ax.set_aspect(1)

    ax.scatter (data[:,0], data[:,1], s=0.1, color='r', alpha=0.25)
    
    x,y = W[...,0], W[...,1]
    
    # add edges of lattice
    ax.add_line(plt.Line2D(x[0,:], y[0,:], 'b', alpha=.5, lw = 3))
    ax.add_line(plt.Line2D(x[-1,:], y[-1,:], 'b', alpha=.5, lw = 3))
    ax.add_line(plt.Line2D(x[:,0], y[:,0], 'b', alpha=.5, lw = 3))
    ax.add_line(plt.Line2D(x[:,-1], y[:,-1], 'b', alpha=.5, lw = 3))

    
    # plot lines in the X direction
    for i in range(1, W.shape[0]-1):
        ax.plot (x[i,:], y[i,:], 'b', alpha=.5)
    
    
    # plot lines in the Y direction
    for i in range(1, W.shape[1]-1):
        ax.plot (x[:,i], y[:,i], 'b', alpha=.5)
    
    # plot the X-Y points
    ax.scatter (x.flatten(), y.flatten(), s=10, color='r', lw=3)
    
    #ax2 = fig.add_subplot(212)
    x = [[[i, j] for j in xrange(1, 7)] for i in xrange(1, 7)]
    y = [[[i, j] for j in xrange(1, 7)] for i in xrange(1, 7)]
    # add edges of lattice
    #ax2.add_line(plt.Line2D([i for i in xrange(0, 8)], 
    #                       [0 for i in xrange(0, 8)], 'b', alpha=.5, lw = 3))
    #ax2.add_line(plt.Line2D([0 for i in xrange(0, 8)], 
    #                       [i for i in xrange(0, 8)], 'b', alpha=.5, lw = 3))
    #ax2.add_line(plt.Line2D([i for i in xrange(0, 8)], 
    #                       [8 for i in xrange(0, 8)], 'b', alpha=.5, lw = 3))
    #ax2.add_line(plt.Line2D([8 for i in xrange(0, 8)], 
    #                       [i for i in xrange(0, 8)], 'b', alpha=.5, lw = 3))
    # plot lines in the X direction
    #for i in range(1, W.shape[0]-1):
    #    ax2.plot (x[i,:], y[i,:], 'b', alpha=.5)
    
    
    # plot lines in the Y direction
    #for i in range(1, W.shape[1]-1):
    #    ax2.plot (x[:,i], y[:,i], 'b', alpha=.5)
    
    # plot the X-Y points
    #ax2.scatter (x.flatten(), y.flatten(), s=10, color='r', lw=3)
   
    plt.show()    
    





alpha = 1.00
momentum = 0.001
momentum = 0.00
learning_steps = 2500000;
monitor = learning_steps/100;



transfer = np.vectorize(lambda slope, o: slope * o)
transfer_der = np.vectorize(lambda slope, o: 1)
error = None
error_der = None
slope = 1
wrange = [-.5, .5]
weights_method = None
inputs = 2
som_shape = (10, 10, 2)




#patterns = np.random.uniform(size=(2, 4000))
#title = "GHA Generalized learning"
X = np.random.uniform(size=(4000, 2))


W, used_patterns, history = SOMLearn(X, alpha, som_shape, learning_steps = learning_steps, 
            init_sigma = .75, monitor = monitor, wrange = wrange, 
            assess_error = assess_error, plotter = None)

keys = history.keys()

title = r'''SOM Lattice for data %d points selected from a random distribution'''%(4000)# +\
            #'''learning rate = %0.4f and\n learning steps completed = %d'''%(alpha, max(keys)) 


create_plot(W, used_patterns, title)

  