'''
Created on Feb 24, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''
from matplotlib import pyplot as plt
from nnwork.annplots.scalarplotter import ANNPlotter
from nnwork.annplots.hw5plots import plot_error_combined, plot_prediction,\
    plot_all_errors

import math
import numpy as np
from nnwork.ann.backprop import BPLearn, BPRecall, BPRecallHistory

# working weights: W = [[ 2.62720762  1.251212   -9.32139497], 
#[-0.26181578 -0.57541867  0.50073973]]

# working weights: V = matrix([[ 0.21801978],
#        [ 1.15031902],
#        [-0.59742359],
#        [ 0.75972187]])







NORMALIZE = lambda x: (x - np.mean(x) ) / np.var( x ); 
SIGNAL_FN = np.vectorize(lambda x: 2 * np.sin(2*np.pi*x/20));
TEST_SIGNAL1_FN = np.vectorize(lambda x: .8 * 
                        np.sin(2*np.pi*x/10) + .25 * np.cos(2*np.pi*x/25));
TEST_SIGNAL2_FN = lambda sz: NORMALIZE( np.random.uniform(-10, 10, (sz, 1)) );


scale_down = np.vectorize(lambda  w: w/2.5);
scale_up = lambda d: 2.5 * d ;
scale_term = lambda min_val, max_val:  .5 * (max_val - min_val);


def error_t(t, o):
    return np.mean(.5 * np.power((o - t), 2))

def rms(e):
    e = np.mean(e)
    return np.power(e, .5)


def rms_t(t, o):
    return rms(np.power(t - o, 2))


epsilon = .0000015
E_last = 0
def assess_error(t, o):
    global epsilon, E_last
    y = o.copy()
    err = np.sum(np.power(( t - y), 2)) - np.var(y) 
    
    # actual assessment is here
    if np.abs(E_last - err) < epsilon and E_last < 1:
        return True
    E_last = err
    return False



INCREMENT_VAL = 1;
i = 1
p = 44;
training_inputs = np.matrix([i*.5 for i in 
                             xrange(1, p+1, INCREMENT_VAL)]).transpose()


training_outputs = SIGNAL_FN(training_inputs);
trainingPatterns =  np.bmat([training_inputs, scale_down(training_outputs)])

test1_inputs = training_inputs.copy()
test1_outputs = TEST_SIGNAL1_FN(test1_inputs);
testPatterns = np.bmat([test1_inputs, scale_down(test1_outputs)])

test2_inputs = np.floor(np.random.uniform(1, 120, (training_inputs.shape[0], 1)));
test2_outputs = TEST_SIGNAL2_FN(test2_inputs.shape[0]);
testPatterns2 = np.bmat([test2_inputs, scale_down(test2_outputs)])


# activation criteria
slope = 1
tanh = np.vectorize(math.tanh)
transfer = np.vectorize(lambda slope, o: tanh(slope * o))
transfer_der = np.vectorize(lambda slope, o: slope * (1 - 
                                            tanh(slope * o) * tanh(slope * o)))

error = None
error_der = None

# network configuration
inputs = 1
outputs = 1
hidden = 4
bias = [1, 1]
wrange = [-.5, .5]
weights_method = None

# learning parameters
alpha = [.015, .015];
momentum = [0.0, 0.0];

learning_steps = 500000;
monitor = 500;
epoch = 1# \trainingPatterns.shape[0]

title = r'''1-1-1 ANN Learning the signal of $sin(\frac{2{\pi}x}{20})$:''' +"\n" +\
            r''' Training and Error History Training for $\alpha$ = %0.4f, epoch size = %d''' 


plotter = ANNPlotter([0, 23], [-3, 3], [0, learning_steps], [0, 1], scale_up, 
                     title%(alpha[0], epoch))

plotter.set_targets(trainingPatterns)
# Known Final Values
W_f = np.matrix([[ 1.36577625, -0.75200461],
 [-1.04194014,  2.22766534],
 [-1.02425208,  2.16026472]])

V_f = np.matrix([[-1.51218265],
 [ 2.10001688],
 [ 2.00265021]])


# Known Initial Values

#patterns = np.matrix( [[0, 0, -1], [0, 1, 1], [1, 0, 1],[1, 1, -1]])

# trainpatterns need to be either {-1, 1}
#trainingPatterns = patterns


W, V, history = BPLearn(trainingPatterns, alpha, inputs, outputs, hidden, 
                         learning_steps=learning_steps, epoch=epoch, 
                         momentum=momentum, transfer=transfer, 
                         transfer_der=transfer_der, error=error, 
                         error_der=error_der, monitor=monitor, slope=slope, 
                         weights_method=weights_method, bias=bias, 
                         wrange=wrange, 
                         assess_error = assess_error, random = 1, 
                         plotter = plotter)

plotter.init_animation("../anim_signalproc_epoch_%d_alpha_%.04f.mp4"%(epoch, alpha[0]))
plt.clf()


output_history, error_history = BPRecallHistory(trainingPatterns[:,:inputs], 
                trainingPatterns[:,inputs:], history, bias, 
                transfer, slope, error_t)


#ax = fig.add_subplot(311)
Y_train = scale_up(BPRecall(trainingPatterns[:,:inputs], W, V, 
            transfer = transfer, slope = slope, bias = bias))

T_train = [trainingPatterns[i, 1] for i in xrange(0, 
                                        trainingPatterns.shape[0])]

X = [trainingPatterns[i, 0] for i in xrange(0, 
                                        trainingPatterns.shape[0])]

Y_test = scale_up(BPRecall(testPatterns[:,:inputs], W, V, transfer = transfer, 
            slope = slope, bias = bias))

T_test = [testPatterns[i, 1] for i in xrange(0, testPatterns.shape[0])]

Y_test2 = scale_up(BPRecall(testPatterns2[:,:inputs], W, V, transfer = transfer, 
            slope = slope, bias = bias))

T_test2 = [testPatterns2[i, 1] for i in xrange(0, testPatterns2.shape[0])]




E_test = rms_t (T_test, Y_test)
E_test2 = rms_t (T_test2, Y_test2)
E_train = rms_t (T_train, Y_train)


epats = {'training':trainingPatterns, 
         'test':testPatterns, 'test2':testPatterns2}



plot_error_combined(epats, error_t, history, transfer, bias, slope, inputs, 
                    scale_up, epoch)
plot_prediction(X, Y_train, Y_test, Y_test2)
plot_all_errors(X, Y_train, Y_test, Y_test2)
