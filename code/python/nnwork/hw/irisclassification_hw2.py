'''
Created on Feb 25, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''
import math
import numpy as np
from nnwork.ann.backprop import BPLearn, BPRecall, BPRecallHistory



def rms_t(t, o):
    return rms(t - o)

def rms(e):
    e = np.mean(np.power(e, 2))
    return np.power(e, .5)


def assess_error(t, o):
    o = post_process_output(o)
    return (t == o).all()

def post_process_output(o):
    o = classify_iris(o)
    return o



def classify_iris(output):
    xi = output.argmax(axis=1);   
    hitv = np.zeros(output.shape);
    j = 0
    for i in xi:
        hitv[j, i] = 1;
        j+= 1
        
    return hitv


def scale_iris_output(scaleme):
    o = classify_iris(scaleme);
    o = np.multiply(o, np.matrix([4, 2, 1]))
    return o.max();


# read in data files
def read_iris_data(fname):
    data = open(fname).read()
    data = data.replace("\n&", " & ")
    data_lines = data.splitlines()
    i = 0
    while i < len(data_lines):
        if data_lines[i].strip().find("Sep_L") == 0:
            i += 1
            break
        i+= 1
    
    data = data_lines[i:]
    patterns = None
    for i in data:
        line = [float(j.strip()) for j in i.split() if j.strip() != '&']
        if patterns is None:
            patterns = np.matrix([line])
        else:
            patterns = np.bmat([[patterns], [np.matrix([line])]])
    
    return patterns
    
data_hdr = "Sep_L  Sep_W  Pet_L  Pet_W Setosa Versacolor Virginica".split()
test_data = read_iris_data("../../iris-test.txt")
train_data = read_iris_data("../../iris-train.txt")




# activation criteria
slope = 1
tanh = np.vectorize(math.tanh)
transfer = np.vectorize(lambda slope, o: tanh(slope * o))
transfer_der = np.vectorize(lambda slope, o: slope * (1 - 
                                            tanh(slope * o) * tanh(slope * o)))

error = None
error_der = None

# network configuration
inputs = 4
outputs = 3
hidden = 3
bias = [1, 1]
wrange = [-.1, .1]
weights_method = None

# learning parameters
alpha = [.01, .01];
momentum = [0, 0];
epsilon = .05;
learning_steps = 6000;
monitor = 100;
epoch = 75

# set up patterns
trainingPatterns = train_data.copy()
trainingPatterns[:, inputs: ]=  -1 * (trainingPatterns[:, inputs:] == 0).astype(np.int8) +\
                          (trainingPatterns[:, inputs:] != 0).astype(np.int8)


testPatterns = test_data.copy()
testPatterns[:, inputs:] = -1 * (testPatterns[:, inputs:] == 0).astype(np.int8) +\
                          (testPatterns[:, inputs:] != 0).astype(np.int8)



W, V, history = BPLearn(trainingPatterns, alpha, inputs, outputs, hidden, 
                         learning_steps=learning_steps, epoch=epoch, 
                         momentum=momentum, transfer=transfer, 
                         transfer_der=transfer_der, error=error, 
                         error_der=error_der, monitor=monitor, slope=slope, 
                         weights_method=weights_method, bias=bias, wrange=wrange, 
                         epsilon=epsilon, assess_error = assess_error)

Y = BPRecall(trainingPatterns[:,:inputs], W, V, transfer = transfer, 
            slope = slope, bias = bias)

output_history, error_history = BPRecallHistory(trainingPatterns[:,:inputs], 
                trainingPatterns[:,inputs:], history, bias, transfer, slope, error)

errors = []
steps = output_history.keys()
steps.sort()

for step in steps:
    errors.append(rms_t(trainingPatterns[:,inputs:], output_history[step]))

errors = [np.mean(error_history[k]) for k in steps]
    
#plt.figure(1)
#plt.subplot(211)
#plt.plot(steps, errors, 'bo', steps, errors, 'k')


print("Final Input Weights: \n %s"% ( str(W)))
print("Final Input Weights: \n %s"% str(V))

print("Input Patterns: \n %s \n %s"%(str(np.mat(data_hdr)), str(trainingPatterns)))

#print("Output Patterns: %s"% str(process_output(Y))) 
iris_actual = classify_iris( Y )
iris_desired = train_data[:, inputs:]

num_equal_by_row = np.sum((iris_actual == iris_desired).astype(np.int8), axis=1)
num_eq_3 = (num_equal_by_row[:, :] == 3).astype(np.int8)
hitrate = float(np.sum(num_eq_3)) / iris_actual.shape[0]  



print "Hitrate: %f"%(hitrate)