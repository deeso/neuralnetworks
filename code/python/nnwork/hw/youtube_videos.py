'''
Created on Feb 24, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''
import math
import numpy as np
from nnwork.annplots.scalarplotter import ANNPlotter
from matplotlib import pyplot as plt

from nnwork.ann.backprop import BPLearn, BPRecall

# working weights: W = [[ 2.62720762  1.251212   -9.32139497], 
#[-0.26181578 -0.57541867  0.50073973]]

# working weights: V = matrix([[ 0.21801978],
#        [ 1.15031902],
#        [-0.59742359],
#        [ 0.75972187]])







NORMALIZE = lambda x: (x - np.mean(x) ) / np.var( x ); 
SIGNAL_FN = np.vectorize(lambda x: 2 * np.sin(2*np.pi*x/20));
TEST_SIGNAL1_FN = np.vectorize(lambda x: .8 * 
                        np.sin(2*np.pi*x/10) + .25 * np.cos(2*np.pi*x/25));
TEST_SIGNAL2_FN = lambda sz: NORMALIZE( np.random.uniform(-10, 10, (sz, 1)) );


scale_down = np.vectorize(lambda  w: w/2.5);
scale_up = lambda d: 2.5 * d ;
scale_term = lambda min_val, max_val:  .5 * (max_val - min_val);


def error_t(t, o):
    return np.mean(.5 * np.power((o - t), 2))

def rms(e):
    e = np.mean(e)
    return np.power(e, .5)


def rms_t(t, o):
    return rms(np.power(t - o, 2))



epsilon = .015
E_last = 0
def assess_error(t, o):
    global epsilon, E_last
    y = o.copy()
    err = np.sum(np.power(( t - y), 2)) - np.var(y) 
    close = np.abs( t - y) <= .005
    # actual assessment is here
    E_last = err
    return close.all() and np.abs(err) <= epsilon



INCREMENT_VAL = 1;
i = 1
p = 44;
training_inputs = np.matrix([i*.5 for i in 
                             xrange(1, p+1, INCREMENT_VAL)]).transpose()


training_outputs = SIGNAL_FN(training_inputs);
trainingPatterns =  np.bmat([training_inputs, scale_down(training_outputs)])

test1_inputs = training_inputs.copy()
test1_outputs = TEST_SIGNAL1_FN(test1_inputs);
testPatterns = np.bmat([test1_inputs, scale_down(test1_outputs)])

test2_inputs = np.floor(np.random.uniform(1, 120, (training_inputs.shape[0], 1)));
test2_outputs = TEST_SIGNAL2_FN(test2_inputs.shape[0]);
testPatterns2 = np.bmat([test2_inputs, scale_down(test2_outputs)])


# activation criteria
slope = 1
tanh = np.vectorize(math.tanh)
transfer = np.vectorize(lambda slope, o: tanh(slope * o))
transfer_der = np.vectorize(lambda slope, o: slope * (1 - 
                                            tanh(slope * o) * tanh(slope * o)))

error = None
error_der = None

# network configuration
inputs = 1
outputs = 1
hidden = 4
bias = [1, 1]
wrange = [-.5, .5]
weights_method = None

# learning parameters
momentum = [0.001, 0.001];

learning_steps = 1500000;
monitor = 20000;
y_maxerr = 500
epoch = trainingPatterns.shape[0]

plotter = ANNPlotter([0, 23], [-3, 3], [0, learning_steps], [0, y_maxerr], scale_up)
plotter.set_targets(trainingPatterns)
# Known Final Values
#W_f = np.matrix([[ 1.36577625, -0.75200461],
# [-1.04194014,  2.22766534],
# [-1.02425208,  2.16026472]])
#
#V_f = np.matrix([[-1.51218265],
# [ 2.10001688],
# [ 2.00265021]])
#
title = r'''1-1-1 ANN Learning the signal of $sin(\frac{2{\pi}x}{20})$: Training and Error History''' +"\n" +\
            r'''$\alpha$ = %0.4f, epoch size = %d, momentum = %0.03f''' 


test_title = r'''1-1-1 ANN Learning the signal of $.8sin(\frac{2{\pi}x}{10}) + .25 * cos(\frac{2{\pi}}{25})$: Training and Error History''' +"\n" +\
            r'''$\alpha$ = %0.4f, epoch size = %d, momentum = %0.03f''' 


def do_run(testFname, trainFname, epoch, alpha, momentum, monitor, learning_steps):
    plotter = ANNPlotter([0, 23], [-3, 3], [0, learning_steps], [.01, y_maxerr], scale_up, 
                         title%(alpha[0], epoch, momentum[0]))
    plotter.set_targets(trainingPatterns)
    
    W, V, history = BPLearn(trainingPatterns, alpha, inputs, outputs, hidden, 
                             learning_steps=learning_steps, epoch=epoch, 
                             momentum=momentum, transfer=transfer, 
                             transfer_der=transfer_der, error=error, 
                             error_der=error_der, monitor=monitor, slope=slope, 
                             weights_method=weights_method, bias=bias, 
                             wrange=wrange,  
                             assess_error = assess_error, random = 1, 
                             plotter = plotter)
    
    plotter.init_animation(trainFname)
    plt.clf()
    
    plottertest = ANNPlotter([0, 23], [-3, 3], [0, learning_steps], [.01, y_maxerr], scale_up, 
                         test_title%(alpha[0], epoch, momentum[0]))
    plottertest.set_targets(testPatterns)
    keys = history.keys()
    keys.sort()
    for i in keys:
            W_ = history[i]['W']
            V_ = history[i]['V']
            plottertest.update_results(testPatterns[:, :inputs], BPRecall(testPatterns[:, :inputs], 
            W_, V_, transfer, slope, bias), i)
    
    plottertest.init_animation(testFname)
    plt.clf()


#alpha = [.015, .015];
#epoch = trainingPatterns.shape[0]
#learning_steps = 3000;
#monitor = math.floor(learning_steps/125)+1

#do_run("../test_run_train_proc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
#                alpha[0], momentum[0]), 
#       "../test_run_test_proc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
#                alpha[0], momentum[0]), 
#       epoch, alpha, momentum, monitor, learning_steps)



alpha = [.015, .015];
epoch = trainingPatterns.shape[0]
learning_steps = 2500000;

# want to record 125 places
monitor = math.floor(learning_steps/125)+1

do_run("../test_anim_signalproc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
                alpha[0], momentum[0]), 
       "../train_anim_signalproc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
                alpha[0], momentum[0]), 
       epoch, alpha, momentum, monitor, learning_steps)


learning_steps = 2500000;

# want to record 125 places
monitor = math.floor(learning_steps/125)+1

alpha = [.015/(.5*trainingPatterns.shape[0]), .015/(.5*trainingPatterns.shape[0])];
epoch = trainingPatterns.shape[0]

do_run("../test_anim_signalproc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
                alpha[0], momentum[0]), 
       "../train_anim_signalproc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
                alpha[0], momentum[0]), 
       epoch, alpha, momentum, monitor, learning_steps)

alpha = [.015/(trainingPatterns.shape[0]), .015/(trainingPatterns.shape[0])];
epoch = trainingPatterns.shape[0]
momentum = [0.015, 0.015];

do_run("../test_anim_signalproc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
                alpha[0], momentum[0]), 
       "../train_anim_signalproc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
                alpha[0], momentum[0]), 
       epoch, alpha, momentum, monitor, learning_steps)


learning_steps = 600000;
# want to record 125 places
monitor = math.floor(learning_steps/125)+1

alpha = [.015, .015];
epoch = 1
momentum = [0.00, 0.00];

do_run("../test_anim_signalproc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
                alpha[0], momentum[0]), 
       "../train_anim_signalproc_epoch_%d_alpha_%.04f_mom_%.04f.mp4"%(epoch, 
                alpha[0], momentum[0]), 
       epoch, alpha, momentum, monitor, learning_steps)
