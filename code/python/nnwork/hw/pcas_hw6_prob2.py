'''
Created on Mar 9, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''

import numpy as np
from nnwork.ann.sangersgha import GHALearn, center_inputs
from nnwork.ann.princomp import princomp
from nnwork.annplots.pcaplotter import ANNPlotter


    

epsilon = 0.0000001
E_last = None
def assess_error(t, o):
    global E_last, epsilon
    #err = np.abs(np.dot(np.dot(o, o.T), np.eye(o.shape[0], o.shape[1]))  - t)
    y = np.diag(np.dot(o, o.T)) * np.eye(o.shape[0], o.shape[1]).T
    #err = np.sum(np.power(( t - y), 2)) - np.var(y)
    #err = np.abs(y  - t)
    err = np.sum(np.power(( t.T - y), 2))# - np.var(y)
    #print err
    E_last = err
    #print p
    return (err <= epsilon).all()


# read in data files
def read_iris_data(fname):
    data = open(fname).read()
    data = data.replace("\n&", " & ")
    data_lines = data.splitlines()
    i = 0
    while i < len(data_lines):
        if data_lines[i].strip().find("Sep_L") == 0:
            i += 1
            break
        i+= 1
    
    data = data_lines[i:]
    patterns = None
    for i in data:
        line = [float(j.strip()) for j in i.split() if j.strip() != '&']
        if patterns is None:
            patterns = np.matrix([line])
        else:
            patterns = np.bmat([[patterns], [np.matrix([line])]])
    
    return patterns
    
data_hdr = "Sep_L  Sep_W  Pet_L  Pet_W Setosa Versacolor Virginica".split()

#trainFile = "iris-train.txt"
#testFile = "iris-test.txt"
trainFile = "../../iris-train.txt"
testFile = "../../iris-test.txt"

test_data = read_iris_data(testFile)
train_data = read_iris_data(trainFile)

inputs = 4
pcs = 4



#trainFile = "iris-train.txt"
#testFile = "iris-test.txt"
test_data = read_iris_data(testFile)
train_data = read_iris_data(trainFile)


alpha = 0.000000001
learning_steps = 1000000;
monitor = learning_steps/100;
epoch = train_data.shape[0]


transfer = np.vectorize(lambda slope, o: slope * o)
transfer_der = np.vectorize(lambda slope, o: 1)
error = None
error_der = None
slope = 1
wrange = [-.1, .1]
weights_method = None

title = r'''Learning History: Generalized Hebian Algorithm (GHA) for PCA''' +"\n" +\
            r'''$\alpha$ = %0.4f, epoch size = %d'''%(alpha, epoch) 

#title = "GHA Generalized learning"
plotter = ANNPlotter(learning_steps, max_coeff = 1, p = pcs, title = title)


ctrainingPatterns = center_inputs(train_data, inputs)

# create Target elements from the PCAs
pc, z, w = princomp(ctrainingPatterns[:, :inputs])
Y = train_data[:, :inputs] * pc

tPatterns = np.bmat([train_data[:, :inputs], Y])
ctrainingPatterns = center_inputs(train_data, inputs)

W, history = GHALearn(ctrainingPatterns, alpha, inputs, pcs=pcs, 
                         learning_steps=learning_steps, epoch=epoch, 
                         monitor=monitor, slope=slope, 
                         weights_method=weights_method, 
                         wrange=wrange, assess_error = None,
                          random = 1, 
                         plotter = None)

print "Actual PC\n", pc

trainFname = "../pcarunAnim.mp4"
#plotter.init_animation(trainFname)
#plt.clf()
del plotter

print "The Eigen Vectors:"
print W, '\n'
print "Eigen Vectors multiplied by itself:"
print np.dot(W, W.T), '\n'

print "The Eigen Values:"
print np.var(W[:, : ], axis = 1), '\n'




