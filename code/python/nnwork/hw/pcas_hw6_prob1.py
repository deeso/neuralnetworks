'''
Created on Mar 9, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''

import numpy as np

from nnwork.ann.princomp import princomp
from nnwork.ann.sangersgha import center_inputs



# read in data files
def read_iris_data(fname):
    data = open(fname).read()
    data = data.replace("\n&", " & ")
    data_lines = data.splitlines()
    i = 0
    while i < len(data_lines):
        if data_lines[i].strip().find("Sep_L") == 0:
            i += 1
            break
        i+= 1
    
    data = data_lines[i:]
    patterns = None
    for i in data:
        line = [float(j.strip()) for j in i.split() if j.strip() != '&']
        if patterns is None:
            patterns = np.matrix([line]) #@UndefinedVariable
        else:
            patterns = np.bmat([[patterns], [np.matrix([line])]])
    
    return patterns


    
data_hdr = "Sep_L  Sep_W  Pet_L  Pet_W Setosa Versacolor Virginica".split()
trainFile = "../../iris-train.txt"
testFile = "../../iris-test.txt"
test_data = read_iris_data(testFile)
train_data = read_iris_data(trainFile)

inputs = 4
outputs = 3



ctrainingPatterns = center_inputs(train_data, inputs).copy()[:, :inputs]
#train_components = train_data.copy()[:, :inputs]
pc,z,w = princomp(ctrainingPatterns)


print  np.dot(pc, pc.T)

#print pc * w

print "PC's (eigenvectors): \n",pc

print "Eigenvalues: \n",w


