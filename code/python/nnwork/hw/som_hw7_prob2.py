'''
Created on Mar 9, 2012

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''

import matplotlib
from nnwork.hw.patterns import patterns
from nnwork.ann.som import SOMLearn, assess_error
matplotlib.use('Qt4Agg')
from matplotlib import pyplot as plt
import numpy as np


def create_plot(W, data, title):
    fig = plt.figure(figsize=(12,12))
    fig.suptitle(title, fontsize = 20)
    ax = fig.add_subplot(111)
    
    ax.axis([0,9,0,9])
    #ax.set_xticks([])
    #ax.set_yticks([])
    ax.set_aspect(1)
    
    
    x,y = W[...,0], W[...,1]
    
    # add edges of lattice
    ax.add_line(plt.Line2D(x[0,:], y[0,:], 'b', alpha=.5, lw = 3))
    ax.add_line(plt.Line2D(x[-1,:], y[-1,:], 'b', alpha=.5, lw = 3))
    ax.add_line(plt.Line2D(x[:,0], y[:,0], 'b', alpha=.5, lw = 3))
    ax.add_line(plt.Line2D(x[:,-1], y[:,-1], 'b', alpha=.5, lw = 3))
    # plot lines in the X direction
    for i in range(1, W.shape[0]-1):
        ax.plot (x[i,:], y[i,:], 'b', alpha=.5)
    
    
    # plot lines in the Y direction
    for i in range(1, W.shape[1]-1):
        ax.plot (x[:,i], y[:,i], 'b', alpha=.5)
    
    # plot the X-Y points
    
    ax.scatter (data[0][:,0], data[0][:,1], s=0.5, color='r', alpha=0.75, label = "Data Set 1")
    ax.scatter (data[1][:,0], data[1][:,1], s=0.5, color='b', alpha=0.75, label = "Data Set 2")
    ax.scatter (data[2][:,0], data[2][:,1], s=0.5, color='g', alpha=0.75, label = "Data Set 3")
    ax.scatter (data[3][:,0], data[3][:,1], s=0.5, color='k', alpha=0.75, label = "Data Set 4")
    ax.scatter (x.flatten(), y.flatten(), s=3, color='r', lw=3, alpha = .5)
    ax.legend(loc='upper left', markerscale=20)
    
    #ax2 = fig.add_subplot(212)
    # add edges of lattice
    #ax2.add_line(plt.Line2D([i for i in xrange(0, 8)], 
    #                       [0 for i in xrange(0, 8)], 'b', alpha=.5, lw = 3))
    #ax2.add_line(plt.Line2D([0 for i in xrange(0, 8)], 
    #                       [i for i in xrange(0, 8)], 'b', alpha=.5, lw = 3))
    #ax2.add_line(plt.Line2D([i for i in xrange(0, 8)], 
    #                       [7 for i in xrange(0, 8)], 'b', alpha=.5, lw = 3))
    #ax2.add_line(plt.Line2D([7 for i in xrange(0, 8)], 
    #                       [i for i in xrange(0, 8)], 'b', alpha=.5, lw = 3))
    
    # plot lines in the X direction
    #for i in range(1, W.shape[0]-1):
    #    ax2.plot ([i for j in xrange(0, 8)], [j for j in xrange(0, 8)], 'b', alpha=.5)
    
    
    # plot lines in the Y direction
    #for i in range(1, W.shape[1]-1):
    #    ax2.plot ([j for j in xrange(0, 8)], [i for j in xrange(0, 8)], 'b', alpha=.5)
    
    # plot the X-Y points
    #ax2.scatter (x.flatten(), y.flatten(), s=10, color='r', lw=3)
    
    #histo = np.zeros((8,8,4))
    
    #for i in len(data):
    #    # iterating over the data sets here
    #    h = histo[i]
    #    for x,y in data[i]:
    #        # iterating over the weights to see which is the first weight that 
    #        # is less than the current value
    #        for w_x in  range(1, W.shape[0]-1):
    #            for w_y in  range(1, W.shape[1]-1):
    #                if x <= w_x and y <= w_y:
    #                    h[x,y] += 1
    #                    break
    
    
    
    plt.show()    
    





alpha = 1.00
momentum = 0.001
momentum = 0.00
learning_steps = 2500000;
monitor = learning_steps/100;



transfer = np.vectorize(lambda slope, o: slope * o)
transfer_der = np.vectorize(lambda slope, o: 1)
error = None
error_der = None
slope = 1
wrange = [-.5, .5]
weights_method = None
inputs = 3
som_shape = (8, 8, 2)

#variance = 0.1, centered at (7,7)


#(7,7), (0,7), (7,0), and (0,0),

#variance = 0.1, centered at (7,7)
X1 = np.sqrt(0.1) * np.random.uniform(size=(1000, 2)); 
X1[..., 0] = X1[..., 0] + 7
X1[..., 1] = X1[..., 1] + 7 

#variance = 0.1, centered at (7,0)
X2 = np.sqrt(0.1) * np.random.uniform(size=(1000, 2)); 
X2[..., 0] = X2[..., 0] + 7


#variance = 0.1, centered at (0,7)
X3 = np.sqrt(0.1) * np.random.uniform(size=(1000, 2)); 
X3[..., 1] = X3[..., 1] + 7

#variance = 0.1, centered at (0,0)
X4 = np.sqrt(0.1) * np.random.uniform(size=(1000, 2)); 

X = np.bmat([[X1], [X2], [X3], [X4]])

#patterns = np.random.uniform(size=(2, 4000))
#title = "GHA Generalized learning"
#X = np.random.uniform(size=(4000, 2))
#X = patterns

W, used_patterns, history = SOMLearn(X, alpha, som_shape, learning_steps = learning_steps, 
            init_sigma = .75, monitor = monitor, 
            wrange = wrange, assess_error = assess_error, plotter = None)

keys = history.keys()

title = r'''SOM Lattice for data %d points for four different random distributions'''%(4000)# +\
            #'''learning rate = %0.4f and\n learning steps completed = %d'''%(alpha, max(keys)) 


create_plot(W, [X1, X2, X3, X4], title)
