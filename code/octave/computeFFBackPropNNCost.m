function [J_w J_v E Y] = computeFFBackPropNNCost(x, t, W, V, ...
          transfer, transfer_der, alpha_momentum = [.01 .01; .09 .09])
        % perform the back propogation algorithm (vectorized)
        % x: input vector
        % t: desired output vector
        % W: input weight matrix
        % V: hidden layer weight matrix
        % transfer: function handle to the transfer function
        % transfer_der: function handle to the differential of the transfer 
        %               function
        % alpha_momentum: learning rate of the xfer function and momentum, each layer has its own
        %              parameters
        
        % error functions of the back prop
        error = @(o, t) .5 .* (o .- t) .^ 2;
        error_mat = @(o, t) .5 .* (o .- t) * (o .- t)';
        error_der = @(o, t) (o .- t);
        % perform hidden layer calculation
        NET_h = x * W';
        Y_h = transfer(NET_h);

        % perform output layer calculation
        x_2 = Y_h;
        
        % if the matrix columns dont match up,
        % the assumption is there should be a 
        % bias node
        % FIXME: this is a hack, is there a better way?

        if (columns(x_2) == columns(V)-1 )
        	x_2 = [ones(rows(Y_h), 1) Y_h];
        endif
        
        NET = x_2 * V'
        Y = transfer(NET);
        
        E_der = error_der(t, Y);
        % (o-t)
        % calculate the deltas for the output layer, 
        
        delta = E_der .* transfer_der(NET)

        % calculate the deltas for the hidden layer
        
        % if the matrix columns dont match up,
        % the assumption is there should be a 
        % bias node
        % FIXME: this is a hack, is there a better way?
        P = V;

        if (columns(V)-1 == columns(NET_h))
        	 P = V(:, 2:end);	
        end
		delta_h = delta * P .* transfer_der(NET_h);
        J_v = alpha_momentum(1,2) * delta' * x_2
        J_w = alpha_momentum(1,2) * delta_h' * x
        
        if(numel(t) == 1)
            E = error(t, Y)
        else
            E = error_mat(t, Y);
        end