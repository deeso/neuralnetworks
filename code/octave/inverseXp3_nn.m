clc; clear all;
transfer_der = @(net) (1 .- (tanh(net) * tanh(net)'));
transfer = @(net) tanh(net);

error = @(o, t) .5 * ( o - t) .^ 2;

% code scales the inputs to [1, -1]
scale_term = @(min_val, max_val)  .5 * (max_val - min_val);
scale_down = @(w, min_val, max_val) scale_term(min_val, max_val)^-1 .* ...
             (w - scale_term(min_val, max_val) - 1);
scale_up = @(d, min_val, max_val) (d ./ scale_term(min_val, max_val)^-1) .+ ...
             scale_term(min_val, max_val) + 1 ;


BASEDIR = "/Users/apridgen/courseprojects/comp502/neuralnetworks/output/";

% output file naming convention
% %s type of output e.g. train or test
% %f: alpha 
% %d #PEs
OUTFILE = "problem3_%s_%0.3f_%d.csv"; 


% create a 2-layer N-PE ANN
INPUTS = 1;
OUTPUTS = 1;
BIAS0 = 0;
BIAS1 = 1;
HIDDENPES = 10;


% Set the required parameters for the ANN
alpha = .05;
epoch = 200;
do_dump = 1;
dump_cnt = 1000;
%epsilon = .005; espsilon is set further down by the boundary
batch_mode = 1;
steps = 20000;


% Create training and test sets for the ANN
% also set parameters for scaling and decision
testNum = 100;
trainNum = 200;
start = .1;
end_ = 1;
min_val = 1/end_; % x = 1, 1/1
max_val = 1/start; % x = .1, 1/10

% since there are 200 potential values
% set up a decision boundary betweeen each 
% output, and set a hit/miss function with that boundary
BOUNDARY = (200) ^ -1;
epsilon = BOUNDARY;
hit_func = @( o, t)  abs( o - t) <= BOUNDARY;

% generate the training patterns for this network
% this case its trainNum x 2 matrix (x, 1/x)
trainingPatterns  = generateXInversePatterns(trainNum, start, end_);
testPatterns  = generateXInversePatterns(testNum, start, end_);

% scale the patterns for training
trainingPatterns = [trainingPatterns(:, 1) ...
           scale_down(trainingPatterns(:, 2), min_val, max_val)];

testPatterns = [testPatterns(:, 1) ...
           scale_down(testPatterns(:, 2), min_val, max_val)];




dump_cnt = 500;
%epsilon = .005; espsilon is set further down by the boundary
batch_mode = 1;
steps = 20000;

%[iW, iV] = createWeightMatrices(INPUTS, OUTPUTS, i-BIAS1, BIAS0, BIAS1);    
% perform the actual training for the network given the above parameters
%[W, V, trainResults, testResults] = performLearnWithEpoch(trainingPatterns, ...
%          testPatterns, iW, iV, transfer, transfer_der, alpha, 60000, ...
%          epoch, epsilon, do_dump, 15000, INPUTS, OUTPUTS);

%output = sprintf(OUTFILE, "train", alpha, HIDDENPES);
%filename = strcat(BASEDIR, output);
%dumpErrorCsv2( filename, trainingPatterns, trainResults, scale_up, min_val, max_val, hit_func);

% perform the network test
%testResults = performNetworkTesting (testPatterns, W, V, transfer, error);

%output = sprintf(OUTFILE, "test", alpha, HIDDENPES);
%filename = strcat(BASEDIR, output);
%dumpErrorCsv2( filename, testPatterns, testResults, scale_up, min_val, max_val, hit_func);


for i = [5, 10, 15, 20]
    fprintf(stderr, "Performing iterations for alpha = %0.02f, hiddenpes = %d\n", alpha, i);

    [iW, iV] = createWeightMatrices(INPUTS, OUTPUTS, i-BIAS1, BIAS0, BIAS1);    
    % perform the actual training for the network given the above parameters
    [W, V, trainResults, testResults] = performLearnWithEpoch(trainingPatterns, ...
              testPatterns, iW, iV, transfer, transfer_der, alpha, steps, ...
              epoch, epsilon, do_dump, dump_cnt, INPUTS, OUTPUTS);
    
    output = sprintf(OUTFILE, "train", alpha, i);
    filename = strcat(BASEDIR, output);
    dumpErrorCsv2( filename, trainingPatterns, trainResults, scale_up, min_val, max_val, hit_func);
    
    % perform the network test
    %testResults = performNetworkTesting (testPatterns, W, V, transfer, error);
    
    output = sprintf(OUTFILE, "test", alpha, i);
    filename = strcat(BASEDIR, output);
    dumpErrorCsv2( filename, testPatterns, testResults, scale_up, min_val, max_val, hit_func);

endfor


HIDDENPES = 10;
[W, V] = createWeightMatrices(INPUTS, OUTPUTS, HIDDENPES-BIAS1, BIAS0, BIAS1);

for alpha_i = [.0001, .0007, .001, .003, .006, .012 .02]
    fprintf(stderr, "Performing iterations for alpha = %0.02f, hiddenpes = %d\n", ...
            alpha_i, HIDDENPES);
    
    
    % perform the actual training for the network given the above parameters
    [W, V, trainResults, testResults] = performLearnWithEpoch(trainingPatterns, ...
              testPatterns, iW, iV, transfer, transfer_der, alpha_i, steps, ...
              epoch, epsilon, do_dump, dump_cnt, INPUTS, OUTPUTS);
    
    output = sprintf(OUTFILE, "train", alpha_i, HIDDENPES);
    filename = strcat(BASEDIR, output);
    
    dumpErrorCsv2( filename, trainingPatterns, trainResults, scale_up, min_val, max_val, hit_func);
    
    % perform the network test
    %testResults = performNetworkTesting (testPatterns, W, V, transfer, error);
    
    output = sprintf(OUTFILE, "test", alpha_i, HIDDENPES);
    filename = strcat(BASEDIR, output);
    dumpErrorCsv2( filename, testPatterns, testResults, scale_up, min_val, max_val, hit_func);

endfor
