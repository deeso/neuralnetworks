function [ Y ] = predict(X, W, V, transfer)
    x_2 = transfer(X * W');
    if(columns(x_2) == columns(V) -1)
        x_2 = [ones(rows(x_2), 1) x_2];
    endif
    Y = transfer(x_2 * V');


