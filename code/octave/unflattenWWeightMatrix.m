function [W] unflattenWWeightMatrix(weights, epoch, w_sz, v_sz, inputs_sz)
    start = (epoch * (w_sz + v_sz)) ;
    stop = start+ w_sz;
    W = weights(start, stop);
    W = reshape(W, w_sz, inputs_sz );

