function  hitv = find_greatest_component(o)
    hitv = (o >= 1);
    
    if (sum(hitv) != 1)
       [x, xi] = max(o);   
       hitv = zeros(size(o));
       hitv(xi) = 1;
    endif
    hitv(hitv(:) == 0) = -1;
    
