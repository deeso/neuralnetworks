function [V] unflattenVWeightMatrix(weights, epoch, w_sz, v_sz, outputs_sz)
    start = (epoch * (w_sz + v_sz)) + w_sz;
    stop = start+v_sz;
    V = weights(start, stop);
    V = reshape(V, outputs_sz, w_sz);

