function [ W, V] = createWeightMatrices(inputs, outputs, hiddenPEs, ...
     bias0 = 0, bias1 = 0, interval = [-.5, .5])
     % create weight matrices based on the following criteria
     % inputs: scalar, number of inputs
     % outputs: scalar, number of outputs
     % hiddenPEs: number of PEs in the hidden layer
     % bias0: 1 if there is a bias on the inputs
     % bias1: 1 if there is a bias on the hidden layer
     % interval: 1 x 2 matrix with [low, high] range to select weights from
     

W = unifrnd(interval(1), interval(2), hiddenPEs, inputs+bias0);
V =  unifrnd(interval(1), interval(2), outputs, hiddenPEs+bias1);
