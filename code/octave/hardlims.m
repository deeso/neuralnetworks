function [M] = hardlims(M, n = 0)
% if M is > 0 make it one, otherwise make it -1
  M(M >= n) = 1;
  M(M < n ) = -1;
endfunction