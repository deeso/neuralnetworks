clc; clear all;

% transfer function and derivative for the BackProp rule
transfer_der = @(net) (1 .- (tanh(net) .* tanh(net)));
transfer = @(net) tanh(net);

error = @(o, t) .5 * ( o - t) .^ 2;

% code scales the inputs to [1, -1]
scale_term = @(min_val, max_val)  .5 * (max_val .- min_val);
scale_down = @(w, min_val, max_val) -1 * (w(:, :) == 0) + (w(:, :) == 1);
scale_up = @(d, min_val, max_val) scale_iris(d);
min_val = max_val = 0;
% load up the IrisData that has been preprocessed
irisData

% create a 2-layer N-PE ANN
INPUTS = 4;
BIAS0 = 0;
BIAS1 = 1;
OUTPUTS = 3;
HIDDENPES = 10;

[W, V] = createWeightMatrices(INPUTS, OUTPUTS, HIDDENPES-BIAS1, BIAS0, BIAS1);


% Set the required parameters for the ANN
alphas = [0.05 .05; 0 0];
epoch = 30;
do_dump = 1;
dump_cnt = 30;
epsilon = .005;
batch_mode = 1;
steps = 3000;

BASEDIR = "/Users/apridgen/courseprojects/comp502/neuralnetworks/output/";
% output file naming convention
% %s type of output e.g. train or test
% %f: alpha 
% %d #PEs
OUTFILE = "problem51_%s_a1-%0.03f_a2-%0.03f_m1-%0.03f_m2-%0.03f_%d.csv"; 



% since there are 200 potential values
% set up a decision boundary betweeen each 
% output, and set a hit/miss function with that boundary
BOUNDARY = (200) ^ -1;
epsilon = BOUNDARY;



hit_func = @( actual, expected)  sum( (find_greatest_component(actual) >= 0) ...
               == (expected >= 0)) == 3;

trainingPatterns  = IrisTraining;
trainingPatterns(:, 5:end) =  scale_down(trainingPatterns(:, 5:end));
testPatterns  = IrisTest;
testPatterns(:,  5:end) = scale_down(testPatterns(:, 5:end));


% generate the training patterns for this network
% this case its trainNum x 2 matrix (x, 1/x)
% scale the patterns for training


% perform the actual training for the network given the above parameters

[iW, iV] = createWeightMatrices(INPUTS, OUTPUTS, HIDDENPES-BIAS1, BIAS0, BIAS1);    
% perform the actual training for the network given the above parameters

for HIDDENPES = [4,5,6]

	[iW, iV] = createWeightMatrices(INPUTS, OUTPUTS, HIDDENPES-BIAS1, BIAS0, BIAS1);    
	
	for alpha_scalar = [.005, .008, .012, .05, .1, .13]
		alphas(1, :) = alpha_scalar .* ones(1, 2);
		output = sprintf(OUTFILE, "train", alphas(1,1), alphas(1,2), alphas(2,1), ...
				alphas(2,2), HIDDENPES);
		fprintf(stderr, " Performing %s\n", output);
		
		[W, V, trainResults, testResults] = performLearnWithEpoch(trainingPatterns, ...
			  testPatterns, iW, iV, transfer, transfer_der, alphas, steps, ...
			  epoch, epsilon, do_dump, dump_cnt, INPUTS, OUTPUTS);
		
		% write out the training steps 
		output = sprintf(OUTFILE, "train", alphas(1,1), alphas(1,2), alphas(2,1), ...
				alphas(2,2), HIDDENPES);
	
		filename = strcat(BASEDIR, output);
		dumpErrorCsv2( filename, trainingPatterns, trainResults, scale_up, min_val, ...
			  max_val, hit_func, INPUTS, OUTPUTS);
		output = sprintf(OUTFILE, "test", alphas(1,1), alphas(1,2), alphas(2,1), ...
				alphas(2,2), HIDDENPES);
		
		filename = strcat(BASEDIR, output);
		dumpErrorCsv2( filename, testPatterns, testResults, scale_up, min_val, ...
			 max_val, hit_func, INPUTS, OUTPUTS);
	
	endfor
endfor
