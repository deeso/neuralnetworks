function [W, V, trainResults, testResults, weights] = performLearnWithEpoch( ...
    trainPatterns, testPatterns, W, V, transfer, transfer_der, ...
    alpha_momentum = [.01 .01; 0 0], steps = 1, epoch_cnt = 1, ...
    epsilon = .005, do_dump = 0, dump_cnt = 1, inputs = 1, outputs = 1, random = 0)
    % perform backpropogation learning on a 2- layer neural network given initial
    % input and hidden layer weights
    % trainPatterns: test patterns for learning
    % W: input layer weights
    % V: hidden layer weights
    % transfer: transfer function used by the neural network
    % transfer_der: differential of the transfer function for back prop learning
    % alpha_momentum: learning rates for the different layers and then the momentum
    %     [alpha_W alpha_V; momentum_W momentum_V]
    % steps: number of allowed steps in the learning process
    % epoch_cnt: number of applications of the learning rule within each step
    % epsilon: error stopping criteria
    % do_dump: perform learning monitoring
    % dump_cnt: monitor every N steps
    
    weight_records_written = 0;
    test_records_written = 0;
    epsilon_break = 0;  % did all patterns meet stopping criteria?
    steps_completed = 0; % number of steps completed throughout the process
    records_written = 0; % number of records written into the monitor matrix
    computations = 0; % number of actual training elements processed 
                      % e.g. steps * steps_completed + epoch_iteration_val (e.g. j))
                      % its faster to increment each time then multiply and add
    

    
    % constants for the monitor matrix, used for recording stats
    ERROR = 1;
    STEPS = 2;
    EXPECTED = 3;
    INPUT = 4;
    EPOCH = 5;
    ACTUAL = 6;
    % subtract 1 from outputs because we already have a place for it
    NUM_MONITORS = 6+outputs-1;
    
    % every time monitoring is performed, all patterns are assessed 
    trainResults = zeros(NUM_MONITORS, (steps) /dump_cnt * rows(trainPatterns));

    
    % every time monitoring is performed, all patterns are assessed 
    testResults = zeros(NUM_MONITORS, (steps ) /dump_cnt * rows(testPatterns));
    
    % trainResults matrix is the monitoring matrix
    trainResults = zeros(5, steps * rows(trainPatterns));
    
    % these are the gradients from the back-prop 
    % learning rule, these may be accumulated if epoch_cnt > 1
    sum_J_w = zeros(size(W));
    sum_J_v = zeros(size(V));
    
    % used for following the error rates of the patterns
    % if all the patterns have an error < epsilon,
    % learning will terminate early
    E_all = ones(rows(trainPatterns), 1);
    E_diff =  epsilon .* ones(rows(trainPatterns), 1);
    
    
    
    % code to help save weights when stuff is monitor
    flatten_weights = @(w, v) [w(:); v(:)]';

    
    LENGTH_WEIGHTS = length(W(:)) + length(V(:));
    
    
    weights = zeros(1, steps/dump_cnt * LENGTH_WEIGHTS);
    
    
    
    fprintf("The maximum iterations to execute: %i\n", steps);
    
    epochs_completed = 0;
    steps_completed = 0;
    while (steps_completed <= steps)
        sum_J_w &= 0;
        sum_J_v &= 0;        

        for j = 1:epoch_cnt
            if (random)
               p = mod(steps_completed, rows(trainPatterns)) + 1; 
               %floor(unifrnd(1, rows(trainPatterns)+1, 1,1));
            else
                p = mod(steps_completed, rows(trainPatterns)) + 1;
            endif
            x = trainPatterns(p, 1:inputs);
            y = trainPatterns(p, inputs+1:end);
p
            % perform backprop and accumulate the results
            [J_w J_v E Y] = computeFFBackPropNNCost(x, y, W, V, transfer, ...
                transfer_der, alpha_momentum);
        %fprintf(stderr, "Before sume\n");
        %fprintf(stderr, "%s\n", mat2str(sum_J_w));
        %fprintf(stderr, "%s\n", mat2str(sum_J_v));


            sum_J_w = sum_J_w .+ (J_w);
            sum_J_v = sum_J_v .+ (J_v);
        %fprintf(stderr, "After sume\n");
        %fprintf(stderr, "%s\n", mat2str(sum_J_w));
        %fprintf(stderr, "%s\n", mat2str(sum_J_v));

            steps_completed += 1;
            % perform dump on every N, number of learning steps
            if (do_dump && ( mod(steps_completed, dump_cnt) == 0))
                x = 0;
                for p = 1:rows(trainPatterns)
                    x = trainPatterns(p, 1:inputs);
                    y = trainPatterns(p, inputs+1:end);
                    [J_w J_v E Y] = computeFFBackPropNNCost(x, y, W, V, transfer, ...
                          transfer_der, alpha_momentum);
                    %fprintf(stderr, "X = %0.04f y = %0.04f\n", x, y);
                    %fprintf(stderr, "X = %0.04f Y = %0.04f\n", x, Y);
    
                    E_diff(p) = abs(E_all(p) - E);
                    E_all(p) = E;
                    trainResults(INPUT, records_written+p) = p;
                    trainResults(EPOCH, records_written+p) = epochs_completed;
                    trainResults(STEPS, records_written+p) = steps_completed;
                    trainResults(ERROR, records_written+p) = E;
                    trainResults(EXPECTED, records_written+p) = p;
                    % trainResults is a column based matrix, output is rows?
                    trainResults(ACTUAL:ACTUAL+outputs, records_written+p) = Y;
                endfor
                start = (weight_records_written*LENGTH_WEIGHTS)+1;
                stop = (weight_records_written+1)*LENGTH_WEIGHTS;

                %fprintf(stderr, "writing to weights(1, %d:%d)\n", start, stop );    
                %fprintf(stderr, "writing to size(%d, %d)\n", size(flattenWeights(W, V)) );
                %weights(1, start:stop) = flattenWeights(W, V);
                %fprintf(stderr, "writing to weights(1, %d) = %d\n", start, weights(1,start) );    
                %fprintf(stderr, "writing to weights(1, %d) = %d\n", stop, weights(1,stop) );
                weight_records_written += 1;

                for p = 1:rows(testPatterns)
                    x = testPatterns(p, 1:inputs);
                    y = testPatterns(p, inputs+1:end);

                    [J_w J_v E Y] = computeFFBackPropNNCost(x, y, W, V, transfer, ...
                          transfer_der, alpha_momentum);
                    %fprintf(stderr, "X = %0.04f y = %0.04f\n", x, y);
                    %fprintf(stderr, "X = %0.04f Y = %0.04f\n", x, Y);    
                    testResults(INPUT, test_records_written+p) = p;
                    testResults(EPOCH, test_records_written+p) = epochs_completed;
                    testResults(STEPS, test_records_written+p) = steps_completed;
                    testResults(ERROR, test_records_written+p) = E;
                    testResults(EXPECTED, test_records_written+p) = p;
                    % trainResults is a column based matrix, output is rows?
                    testResults(ACTUAL:ACTUAL+outputs-1, test_records_written+p) = Y;

                endfor
                records_written += rows(trainPatterns);
                test_records_written += rows(testPatterns);
            endif
            
            %if (mod(steps_completed, dump_cnt) == 0)
            %    fprintf("Completed %i steps\n", steps_completed);
            %endif

        endfor
        epochs_completed += 1;
        
        W = W .+ sum_J_w;
        V = V .+ sum_J_v;
        
        
        %fprintf(stderr, "%s\n", mat2str(W));
        %fprintf(stderr, "%s\n", mat2str(V));
        
        % check to see if the error of the of all the patterns is
        % less than the number of total max value of the error differentials
        % if so recheck all patterns to verify, and terminate early if this is 
        % the case
        if ( sum(abs(E_diff)) <= epsilon*rows(E_diff) )
            for p_i = 1:rows(trainPatterns)
                x = trainPatterns(p_i, 1:inputs);
                y = trainPatterns(p_i, inputs+1:end);
                [w, v, E] = computeFFBackPropNNCost(x, y, W, V, ...
                         transfer, transfer_der, alpha_momentum);
                if (E > epsilon)
                   E_diff(p_i) = 1+epsilon;
                   break
                endif
            endfor
            if ( sum(E_diff > epsilon) == 0 )
                epsilon_break = 1;
            endif
        endif
        
        if (epsilon_break)
            break;
        endif
        
                
    endwhile
    % remove unused monitor elements if the learning ended early
    trainResults = trainResults(:, 1:records_written);
    testResults = testResults(:, 1:test_records_written);
    fprintf("Completed %i steps and computations %i\n", steps_completed, computations    );
    
    