function scaled = scale_iris(scaleme)
    
    if (sum(abs(scaleme)) == 3)
        scaled = sum((scaleme >= 0) .* [3, 2, 1]);
        return
    endif
    scaleme2 = find_greatest_component(scaleme);
    scaled = sum(    (scaleme2 >= 0) .* [3 2 1] );
    
    
