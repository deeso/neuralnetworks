function [Patterns] = generateXInversePatterns(num, start, end_)
	% generate N inverse X input patterns in the range [start, end_]
	% num is number of patterns to generate
	% start is the start of the range
	% end_ is the end of the range

    rnd_mat = unifrnd(start, end_, num, 1);
    rnd_function = [1./rnd_mat];
    Patterns = [rnd_mat rnd_function];

