function [weights] = flattenWeights(w, v) 
    weights = [w(:); v(:)]';