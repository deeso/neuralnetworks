function dumpErrorCsv(filename, Patterns, resultsMatrix )
% dump out resultsMatrix from the XOR ANN.  It does not use dumpErrorCsv2, because they are learning 
% two different types of values and I could not figure out how to reconcile them.
    
    fid = fopen (filename, "w");
    ERROR = 1;
    STEPS = 2;
    EXPECTED = 3;
    INPUT = 4;
    EPOCH = 5;
    ACTUAL = 6;
    NUM_MONITORS = 6;
    fprintf(fid,"Step,Input,Actual,Hardlim,Expected,results\n");
    rms = 0;
    for i = 1:columns(resultsMatrix)
        step = resultsMatrix(EPOCH, i);
        input = resultsMatrix(INPUT, i);
        actual = resultsMatrix(ACTUAL, i);
        expected = Patterns(resultsMatrix(EXPECTED, i), end);
        error = resultsMatrix(ERROR, i);
        %error = (sum(error) / 2) .^.5;
        hardlim = -1 * (actual < 0) + 1 * (actual >= 0);
        fprintf(fid,"%i,%i,%0.02f,%i,%i,%0.03f\n",step, input, actual, hardlim, ...
                        expected, error);
    end
    fclose(fid);