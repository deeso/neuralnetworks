clc; clear all;
transfer_der = @(net) (1 .- (tanh(net) .* tanh(net)));
transfer = @(net) (tanh(net));

% experimenting with the backprop error functions
% this one seems to have better luck with completing on time

error_der = @(t, o)  (o .- t);
error = @(t, o) .5 * (o .- t) .^ 2;


Patterns  = [ 1 0 0 -1; 
              1 0 1 1; 
              1 1 0 1; 
              1 1 1 -1;];
              
              
% Setup Parameters for the ANN
INPUTS = 2;
BIAS0 = 1;
BIAS1 = 1;
TOTALPES = 7;
OUTPUTS = 1;

HIDDENPES = TOTALPES - BIAS0 - OUTPUTS;

NPE0 = INPUTS + BIAS0 ;
NPE1 = HIDDENPES - NPE0 + BIAS1;

W = unifrnd(-.5, .5, NPE1-1, NPE0);
V =  unifrnd(-.5, .5, OUTPUTS, NPE1);


alpha = [.01 .01; .09 .09];
epoch = 1;
do_dump = 0;
epsilon = .005;
steps = 6000;
dump_cnt = 1;

fprintf("Initial Hidden Layer Weights = %0.02f %0.02f %0.02f\n", W(1,:));
fprintf("Initial Hidden Layer Weights = %0.02f %0.02f %0.02f\n", W(2,:));
fprintf("Initial Output Layer Weights = %0.02f %0.02f %0.02f\n", V);

W = [-0.04 -0.32 0.05; 0.07 0.40 0.31]
V = [ -0.03 -0.14 0.47]

[W, V, trainResults, testResults] = performLearnWithEpoch(Patterns, ...
	          Patterns, W, V, transfer, transfer_der, alpha, steps, ...
	          epoch, epsilon, do_dump, dump_cnt, 3, INPUTS+BIAS0, OUTPUTS);


fprintf("Final Hidden Layer Weights = %0.02f %0.02f %0.02f\n", W(1,:));
fprintf("Final Hidden Layer Weights = %0.02f %0.02f %0.02f\n", W(2,:));
fprintf("Final Output Layer Weights = %0.02f %0.02f %0.02f\n", V);

% run through a simple test of each pattern
for p = 1:rows(Patterns)
    x = Patterns(p, 1:end-1);
    y = Patterns(p, end);
    Y = hardlims(predict(x, W, V, transfer));
    fprintf("x = %d %d %d, y = %d, Y= %d, y == Y = %d\n", x, y, Y, y ==Y);
end

filename = "problem1_results.csv";
dumpErrorCsv( filename, Patterns, trainResults);