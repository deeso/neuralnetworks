function [testError] = performNetworkTesting (Patterns, W, V, transfer, error, ... 
       inputs = 1, outputs = 1)
	% perform the testing of the trained 2-layer neural network 
	% Patterns are the test inputs (:, 1:end-1) and desired outputs (:, end)
	% W is a matrix of input weights 
	% V is the hidden layer weights
	% transfer: transfer function used by the neural network

	% perform the network testing
    ERROR = 1;
    STEPS = 2;
    EXPECTED = 3;
    INPUT = 4;
    EPOCH = 5;
    ACTUAL = 6;
    % subtract 1 from outputs because we already have a place for it
    NUM_MONITORS = 6+outputs-1;

	testError = zeros(6, rows(Patterns));
	
	% loop overall the patterns and perform the testing
	for p = 1:rows(Patterns)
		x = Patterns(p, 1:inputs);
		y = Patterns(p, inputs+1:end);
		Y = predict(x, W, V, transfer);
		if (numel(x) > 1)
		   x = x(2);
		endif
		% record the test results
		testError(STEPS, p) = 0;
		testError(ERROR, p) = error(y, Y);
        % Error is a column based matrix output is rows based
		testError(ACTUAL:ACTUAL+outputs-1, p) = Y';
		testError(EXPECTED, p) = p;
		testError(EPOCH, p) = 1;
		testError(INPUT, p) = p;
	end
