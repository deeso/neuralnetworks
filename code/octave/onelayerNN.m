%Patterns  = [ 1 2.2 1.4 1; % Class A
%              1 1.5 1.0 1; % Class A 
%              1 0.6 0.5 1; % Class A 
%              1 2.3 2.0 -1; % Class B
%              1 1.3 1.5 -1; % Class B
%              1 0.3 1.0 -1; % Class B
%              ];

function [W, E_all] = onelayerNN(Patterns, W, activation, activation_der, ...
         error, alpha = .01, steps = 1, epsilon = .05)
    
    t = 1
    E_all = 100 .* ones(1, rows(Patterns));
    fprintf("Performing %d steps, alpha = %f epsilon = %f\n", steps, alpha, ...
             epsilon);
    for i = 1:steps
        % choose an input pattern, X, and apply it to the I/L
        % set the respective output values

        p = floor(rand() * rows(Patterns)) + 1;
        x = Patterns(p, 1:end-1);
        y = Patterns(p, end);
        %fprintf("Step: %d, Pattern = %d %d %d Output = %d\n", i, x, y);
    
    
        [w, E] = computeFFNNCost(x, y, W, activation, activation_der, ... 
              error, alpha);
    
        fprintf("Step: %d, Gradients Hidden Layer Weights = %f %f %f\n", ...
              i, w(1,:));
        
        W = W .+ w;
        
        E_all(p) = E;
        fprintf("Accumulated Error = %f\n",  mean(E_all));
        if ( sum(E_all > epsilon) == 0 )
            for p = 1:rows(Patterns)
                x = Patterns(p, 1:end-1);
                y = Patterns(p, end);
                [w, E] = computeFFNNCost(x, y, W, activation, ...
                         activation_der, error, alpha);
                E_all(p) = E;
            end
            if ( sum(E_all > epsilon) == 0 )
            	fprintf("Ending the accumulated error is less \
            	         than the stopping criteria");
            	break;
            end
        endif
    end