clc; clear all;
transfer_der = @(net) (1 .- (tanh(net) .* tanh(net)));
transfer = @(net) tanh(net);

error = @(o, t) .5 * ( o - t) .^ 2;

% code scales the inputs to [1, -1]
scale_term = @(min_val, max_val)  .5 * (max_val - min_val);
scale_down = @(w, min_val, max_val) scale_term(min_val, max_val)^-1 .* ...
             (w - scale_term(min_val, max_val) - 1);
scale_up = @(d, min_val, max_val) (d ./ scale_term(min_val, max_val)^-1) .+ ...
             scale_term(min_val, max_val) + 1 ;


% create a 2-layer N-PE ANN
INPUTS = 1;
BIAS1 = 1;
MINPE = 10;
TOTALPES = 12;
OUTPUTS = 1;
HIDDENPES = TOTALPES - OUTPUTS;

NPE0 = INPUTS ;
NPE1 = HIDDENPES - NPE0 + BIAS1;

iW = .5 .* rand(NPE1-BIAS1, NPE0) .+ -1 * round(rand(NPE1-BIAS1, NPE0));
iV = .5 .* rand(OUTPUTS, 10) .+ -1 * round(rand(OUTPUTS, 10));

% Set the required parameters for the ANN
alpha = [.05 .05; 0 0];
epoch = 200;
do_dump = 1;
dump_cnt = 15000;
%epsilon = .005; espsilon is set further down by the boundary
batch_mode = 1;
steps = 60000;


% Create training and test sets for the ANN
% also set parameters for scaling and decision
testNum = 100;
trainNum = 200;
start = .1;
end_ = 1;
min_val = 1/end_; % x = 1, 1/1
max_val = 1/start; % x = .1, 1/10

% since there are 200 potential values
% set up a decision boundary betweeen each 
% output, and set a hit/miss function with that boundary
BOUNDARY = (200) ^ -1;
epsilon = BOUNDARY;
hit_func = @( o, t)  abs( o - t) <= BOUNDARY;

% generate the training patterns for this network
% this case its trainNum x 2 matrix (x, 1/x)
trainingPatterns  = generateXInversePatterns(trainNum, start, end_);
testPatterns  = generateXInversePatterns(testNum, start, end_);

% scale the patterns for training
Patterns = [trainingPatterns(:, 1) ...
		   scale_down(trainingPatterns(:, 2), min_val, max_val)];

Patterns2 = [testPatterns(:, 1) ...
           scale_down(testPatterns(:, 2), min_val, max_val)];

if (INPUTS > 1) 
	Patterns = [ ones(rows(trainingPatterns), 1) Patterns];
	Patterns2 = [ ones(rows(testPatterns), 1)  Patterns2];
endif


% generate the training patterns for this network
% this case its trainNum x 2 matrix (x, 1/x)
% scale the patterns for training

% perform the actual training for the network given the above parameters

[W, V, trainResults, testResults] = performLearnWithEpoch(trainingPatterns, ...
              testPatterns, iW, iV, transfer, transfer_der, alpha, steps, ...
              epoch, epsilon, do_dump, dump_cnt, INPUTS, OUTPUTS);

% dump the monitored parameters to a file for post processing
filename = "problem2_train_results.csv";
dumpErrorCsv2( filename, trainingPatterns, trainResults, scale_up, min_val, max_val, hit_func);

% perform the network test
%testError = performNetworkTesting (Patterns2, W, V, transfer, error);

% dump the monitored parameters to a file for post processing
filename = "problem2_test_results.csv";
dumpErrorCsv2( filename, testPatterns, testResults, scale_up, min_val, max_val, hit_func);
