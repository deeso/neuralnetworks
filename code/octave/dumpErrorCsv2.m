function dumpErrorCsv2(filename, Patterns, resultsMatrix, scale, min_val, max_val, ...
          hit_func, inputs = 1, outputs = 1)
    % dump out the monitor informtion
    % filename: place to write the data to file
    % resultsMatrix: contains all the monitor information, it is a 6 x N matrix, where 
    %        N is the number of dumps
    % scale: lambda used to rescale the inputs with min_val and max_val
    % hit_func: function used to determine if the acutal out put is valid

    fid = fopen (filename, "w");
    ERROR = 1;
    STEPS = 2;
    EXPECTED = 3;
    INPUT = 4;
    EPOCH = 5;
    ACTUAL = 6;
    NUM_MONITORS = 6+outputs-1;
    
    InputStr = ",Input";
    InputFmtStr = ',%0.04f';
    if (inputs > 1)
        InputStr = "";
        InputFmtStr = "";
    	for i = 1:inputs
    	    InputStr = sprintf("%s,Input_%d",InputStr, i);
    	    InputFmtStr = strcat(InputFmtStr,',%0.04f');
    	endfor
    endif
    
    ActualStr = ",Actual";
    ActualFmtStr = ',%0.04f';
    if (outputs > 1)
        ActualStr = "";
        ActualFmtStr = "";
    	for i = 1:outputs
    	    ActualStr = sprintf("%s,Actual_%d",ActualStr, i);
    	    ActualFmtStr = strcat(ActualFmtStr, ',%0.04f');
    	endfor
    endif
    
    ExpectedStr = ",Expected";
    ExpectedFmtStr = ',%0.04f';
    if (outputs > 1)
        ExpectedStr = "";
        ExpectedFmtStr = "";
    	for i = 1:outputs
    	    ExpectedStr = sprintf("%s,Expected_%d",ExpectedStr, i);
    	    ExpectedFmtStr = strcat(ExpectedFmtStr, ',%0.04f');
    	endfor
    endif

     ActualScaledStr = ",ActualScaled";
     ActualScaledFmtStr = ',%0.4f';
%%     if (outputs > 1)
%%         ActualScaledStr = "";
%%         ActualScaledFmtStr = "";
%%     	for i = 1:outputs
%%     	    ActualScaledStr = sprintf("%s,ActualScaled_%d",ActualScaledStr, i);
%%     	    ActualScaledFmtStr = strcat(ActualScaledFmtStr, ',%0.4f');
%%     	endfor
%%     endif
%%     
     ExpectedScaledStr = ",ExpectedScaled";
     ExpectedScaledFmtStr = ',%.4f';
%%     if (outputs > 1)
%%         ExpectedScaledStr = "";
%%         ExpectedScaledFmtStr = "";
%%     	for i = 1:outputs
%%     	    ExpectedScaledStr = sprintf("%s,ExpectedScaled_%d",ExpectedScaledStr, i);
%%     	    ExpectedScaledFmtStr = strcat(ExpectedScaledFmtStr, ',%0.4f');
%%     	endfor
%%     endif
    
    
    % form the output format for the string
    outputStr = "Step,Epoch,Hit";
    outputStr = strcat(outputStr,InputStr,ActualStr,ExpectedStr);
    outputStr = strcat(outputStr,ActualScaledStr,ExpectedScaledStr);
    outputStr = strcat(outputStr,",Error\n");
    fprintf(fid,outputStr);
    
    % output format
    outputFmtStr = "%i,%i,%i";
    outputFmtStr = strcat(outputFmtStr,InputFmtStr,ActualFmtStr,ExpectedFmtStr);
    outputFmtStr = strcat(outputFmtStr,ActualScaledFmtStr,ExpectedScaledFmtStr);
    outputFmtStr = strcat(outputFmtStr,",%0.04f\n");
    
    rms = 0;
    for i = 1:columns(resultsMatrix)
        step = resultsMatrix(STEPS, i);

        input = Patterns(resultsMatrix(INPUT, i), 1:inputs);
        % resultsMatrix is a column based matrix
        actual = (resultsMatrix(ACTUAL:ACTUAL+outputs-1, i))';
        expected = Patterns(resultsMatrix(EXPECTED, i), inputs+1:end);
        error = resultsMatrix(ERROR, i);
        epoch = resultsMatrix(EPOCH, i);
        hit = hit_func(actual, expected);
        actual_ = scale(actual, min_val, max_val);
        expected_ = scale(expected, min_val, max_val);
%%         fprintf(stderr, "input %.2f\n", input);
%%         fprintf(stderr, "actual %0.2f\n", actual_);
%%         fprintf(stderr, "exp %0.2f\n", expected_);
%%         fprintf(stderr, "error %0.2f\n", error);
%%         fprintf(stderr, "%s\n", outputStr);
%%         fprintf(stderr, "%s\n", outputFmtStr);
%%         fprintf(stderr, "%s\n", mat2str(hit));
%%         fprintf(stderr,outputFmtStr, ... 
%%              step, epoch, hit, input, actual, expected, actual_, expected_, error);
%% 
%%         pause;
        fprintf(fid,outputFmtStr, ... 
             step, epoch, hit, input, actual, expected, actual_, expected_, error);
%%        fprintf(fid,outputFmtStr, ... 
%%             step, epoch, hit, input, actual, expected, error);

    end
    fclose(fid);