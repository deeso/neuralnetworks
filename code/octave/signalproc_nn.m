clc; clear all;

BASEDIR = "/Users/apridgen/courseprojects/comp502/neuralnetworks/output/";
% output file naming convention
% %s type of output e.g. train or test
% %f: alpha 
% %d #PEs
OUTFILE = "problem52_%s_a1-%0.03f_a2-%0.03f_m1-%0.03f_m2-%0.03f_%d.csv"; 




SLOPE = 1/6;

% TRANSFER_FN function and derivative for the BackProp rule
TRANSFER_DER_FN = @(net, slope = SLOPE) slope .* ...
                    (1 .- (tanh(slope .* net) .* tanh(slope .* net)));
TRANSFER_FN = @(net, slope = SLOPE) tanh(slope .* net);
SIGNAL_FN = @( x ) 2 * sin(2*pi*x/20);

NORMALIZE = @(x )(x .- mean(x(:)) ) ./ var( x(:) ); 
TEST_SIGNAL1_FN = @( x ) .8 * sin(2*pi*x/10) + .25 * cos(2*pi*x/25);
TEST_SIGNAL2_FN = @( sz ) NORMALIZE( unifrnd(-10, 10, sz, 1) );


error = @(o, t) .5 * ( o - t) .^ 2;

% code scales the inputs to [1, -1]
scale_term = @(min_val, max_val)  .5 * (max_val .- min_val);
scale_down = @(w, min_val, max_val) (2.5^-1) * w;
scale_up = @(d, min_val, max_val) 2.5 * d ;

max_val = min_val = 0;



INCREMENT_VAL = .5;
i = 1
p = 42/.5;
training_inputs = zeros(p, 1);
test1_inputs = zeros(p, 1);

while i <= p+1
    training_inputs(i) = i;
    i += 1;
endwhile

training_outputs = SIGNAL_FN(training_inputs);
trainingPatterns =  [training_inputs scale_down(training_outputs)];

test1_inputs = training_inputs;
test1_outputs = TEST_SIGNAL1_FN(test1_inputs);
testPatterns = [test1_inputs scale_down(test1_outputs)];

test2_inputs = floor(unifrnd(1, 120, rows(training_inputs), 1));
test2_outputs = TEST_SIGNAL2_FN(rows(test2_inputs));
testPatterns2 = [test2_inputs scale_down(test2_outputs)];




% create a 2-layer N-PE ANN
INPUTS = 1;
BIAS0 = 0;
BIAS1 = 1;
OUTPUTS = 1;
HIDDENPES = [4, 80];
WEIGHT_INTERVAL = [-.1, .1];



% Set the required parameters for the ANN
alphas = [0.01 .01; .0 .0];
epoch = rows(trainingPatterns);
do_dump = 1;
dump_cnt = 2000;
batch_mode = 1;
steps = 200000;
epsilon = .0015;



hit_func = @( actual, expected)  (actual - expected) <  epsilon;



% perform the actual training for the network given the above parameters


for hiddenpes = HIDDENPES

    [iW, iV] = createWeightMatrices(INPUTS, OUTPUTS, hiddenpes-BIAS1,  ...
       BIAS0, BIAS1, WEIGHT_INTERVAL);    
    
    for alpha_scalar = [.008, .01]
        alphas(1, :) = alpha_scalar .* ones(1, 2);
        output = sprintf(OUTFILE, "train_2", alphas(1,1), alphas(1,2), alphas(2,1), ...
                alphas(2,2), hiddenpes);
        fprintf(stderr, " Performing %s\n", output);
        
        [W, V, trainResults, testResults, weights] = performLearnWithEpoch(trainingPatterns, ...
              testPatterns, iW, iV, TRANSFER_FN, TRANSFER_DER_FN, alphas, steps, ...
              epoch, epsilon, do_dump, dump_cnt, INPUTS+BIAS0, OUTPUTS);
        
        % write out the training steps 
        output = sprintf(OUTFILE, "train_2", alphas(1,1), alphas(1,2), alphas(2,1), ...
                alphas(2,2), hiddenpes);
        
        % dump out the training results
        filename = strcat(BASEDIR, output);
        dumpErrorCsv2( filename, trainingPatterns, trainResults, scale_up, min_val, ...
              max_val, hit_func, INPUTS, OUTPUTS);
        output = sprintf(OUTFILE, "test_2", alphas(1,1), alphas(1,2), alphas(2,1), ...
                alphas(2,2), hiddenpes);
        
        % dump out the test results
        filename = strcat(BASEDIR, output);
        dumpErrorCsv2( filename, testPatterns, testResults, scale_up, min_val, ...
             max_val, hit_func, INPUTS, OUTPUTS);
        
        % inefficient, but retrain the network again to get the 2nd test results
        [W, V, trainResults, testResults] = performLearnWithEpoch(trainingPatterns, ...
              testPatterns2, iW, iV, TRANSFER_FN, TRANSFER_DER_FN, alphas, steps, ...
              epoch, epsilon, do_dump, dump_cnt, INPUTS+BIAS0, OUTPUTS);
        
        filename = strcat(BASEDIR, output);
        output = sprintf(OUTFILE, "test2_2", alphas(1,1), alphas(1,2), alphas(2,1), ...
                alphas(2,2), hiddenpes);
        
        filename = strcat(BASEDIR, output);
        dumpErrorCsv2( filename, testPatterns2, testResults, scale_up, min_val, ...
             max_val, hit_func, INPUTS, OUTPUTS);

    endfor
endfor




INCREMENT_VAL = .175;
i = 1
k = 1
p = 22/INCREMENT_VAL;
training_inputs = zeros(p, 1);
test1_inputs = zeros(p, 1);

while i <= p+1
    training_inputs(i) = k+INCREMENT_VAL;
    k += INCREMENT_VAL;
    i += 1;
endwhile

training_outputs = SIGNAL_FN(training_inputs);
trainingPatterns =  [training_inputs scale_down(training_outputs)];

test1_inputs = training_inputs;
test1_outputs = TEST_SIGNAL1_FN(test1_inputs);
testPatterns = [test1_inputs scale_down(test1_outputs)];

test2_inputs = floor(unifrnd(1, 120, rows(training_inputs), 1));
test2_outputs = TEST_SIGNAL2_FN(rows(test2_inputs));
testPatterns2 = [test2_inputs scale_down(test2_outputs)];

for hiddenpes = HIDDENPES

    [iW, iV] = createWeightMatrices(INPUTS, OUTPUTS, hiddenpes-BIAS1,  ...
       BIAS0, BIAS1, WEIGHT_INTERVAL);    
    
    for alpha_scalar = [.008, .01]
        alphas(1, :) = alpha_scalar .* ones(1, 2);
        output = sprintf(OUTFILE, "train_2", alphas(1,1), alphas(1,2), alphas(2,1), ...
                alphas(2,2), hiddenpes);
        fprintf(stderr, " Performing %s\n", output);
        
        [W, V, trainResults, testResults, weights] = performLearnWithEpoch(trainingPatterns, ...
              testPatterns, iW, iV, TRANSFER_FN, TRANSFER_DER_FN, alphas, steps, ...
              epoch, epsilon, do_dump, dump_cnt, INPUTS+BIAS0, OUTPUTS);
        
        % write out the training steps 
        output = sprintf(OUTFILE, "train_2", alphas(1,1), alphas(1,2), alphas(2,1), ...
                alphas(2,2), hiddenpes);
        
        % dump out the training results
        filename = strcat(BASEDIR, output);
        dumpErrorCsv2( filename, trainingPatterns, trainResults, scale_up, min_val, ...
              max_val, hit_func, INPUTS, OUTPUTS);
        output = sprintf(OUTFILE, "test_2", alphas(1,1), alphas(1,2), alphas(2,1), ...
                alphas(2,2), hiddenpes);
        
        % dump out the test results
        filename = strcat(BASEDIR, output);
        dumpErrorCsv2( filename, testPatterns, testResults, scale_up, min_val, ...
             max_val, hit_func, INPUTS, OUTPUTS);
        
        % inefficient, but retrain the network again to get the 2nd test results
        [W, V, trainResults, testResults] = performLearnWithEpoch(trainingPatterns, ...
              testPatterns2, iW, iV, TRANSFER_FN, TRANSFER_DER_FN, alphas, steps, ...
              epoch, epsilon, do_dump, dump_cnt, INPUTS+BIAS0, OUTPUTS);
        
        filename = strcat(BASEDIR, output);
        output = sprintf(OUTFILE, "test2_2", alphas(1,1), alphas(1,2), alphas(2,1), ...
                alphas(2,2), hiddenpes);
        
        filename = strcat(BASEDIR, output);
        dumpErrorCsv2( filename, testPatterns2, testResults, scale_up, min_val, ...
             max_val, hit_func, INPUTS, OUTPUTS);

    endfor
endfor
