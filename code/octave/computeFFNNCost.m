function [J E] = computeFFNNCost(x, y, W, activation, activation_der, ...
       error, alpha = .01)
        % perform hidden layer calculation
        NET = x * W';
        Y = activation(NET);
    
        % calculate the deltas for the output layer, 
        delta = error(y, Y) * activation_der(Y);
    
        J = alpha .* delta' * x;
		E = error(y, Y)


