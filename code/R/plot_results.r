library (ggplot2)


scale_term <- function(min_val, max_val){
    return(.5 * (max_val - min_val));
}
scale_up <- function(value, min_val = 1, max_val = 10){
    return( (value / scale_term(min_val, max_val)^-1) + 
           scale_term(min_val, max_val) + 1)
}

rmse <- function(obs, pred) sqrt(mean((obs-pred)^2))


combine_data <- function(test_data, train_data){
    
    p2_test_data <- read.csv(test_data, stringsAsFactors=F)
    p2_train_data <- read.csv(train_data, stringsAsFactors=F)
    
    p2_test_data$ScaledActual <- scale_up(p2_test_data$Actual)
    p2_test_data$ScaledExpected <- scale_up(p2_test_data$Expected)
    p2_test_data$run <- "Test"
    
    p2_train_data$ScaledActual <- scale_up(p2_train_data$Actual)
    p2_train_data$ScaledExpected <- scale_up(p2_train_data$Expected)
    p2_train_data$run <- "Train"
    p2_all <- rbind(p2_train_data, p2_test_data)
    return(p2_all)
}

create_merged_data_set <- function(test_data, train_data){
    
    p2_all <- combine_data(test_data, train_data)
    records <- unique((p2_all$Epoch))
    lrecords <- length(records)
    
    selection <- c(records[1], records[lrecords/4], 
                records[lrecords/2], records[lrecords*.75], 
                records[lrecords])
    
    p2_subset_all <- subset(p2_all, Epoch %in% selection)
    p2_subset_all$value <- p2_subset_all$ScaledActual
    
    p2_expected <- p2_subset_all
    p2_expected$value <- p2_expected$ScaledExpected
    p2_expected$run <- "Expected"
    
    p2_subset_all <- rbind(p2_subset_all, p2_expected)
    return(p2_subset_all)

}

create_combined_plot <- function(data_input, Title){
    data_input$Input
    train_input_output_plot <- ggplot() +  
        geom_line(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
        geom_point(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
          ylab("Neural Network Output Values") + 
          xlab("Neural Network Input Values") + 
          scale_colour_manual("Data Point Types",  values = c("Expected" = "black", "Train" = "red", "Test" = "blue")) +
          geom_rug(data = subset(data_input, Hit == 1), mapping = aes(x = Input, y = NULL, color = run)) +
          facet_grid(Epoch ~ .) +   
      opts(title = Title,
      title.position=c(.5,0.9))
    return(train_input_output_plot)
}


create_error_hist_plot <- function(data_input, Title){

    data_input <- ddply(data_input, .( Epoch, run ), mutate, 
                     value = rmse(Actual, Expected))
    data_input2 <-  data.frame(Epoch = data_input$Epoch, 
                        value = data_input$value, run = data_input$run)
    
    error_plot <- ggplot(data = data_input2, mapping = aes(x = Epoch, 
        y=value, color = run)) +  
        geom_line() + 
          geom_point() + 
          ylab("Neural Network Error Values") + 
          xlab("Neural Network Epochs (Epoch Size = 200)") + 
          scale_colour_manual("Data Point Types",  values = c("Train" = "red", "Test" = "blue")) +
      opts(title = Title,
      title.position=c(.5,0.9))
    return(error_plot)
}

test_data <- "problem3_test_0.005_10.csv"
train_data <- "problem3_train_0.005_10.csv"

results_title = "Inverse X Output Training versus Test versus Expected for Given Epoch\n Configuration = 1-10-1, alpha = .005, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-10-1, alpha = .005, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

tdata <- create_merged_data_set(test_data, train_data)
tplot <- create_combined_plot(tdata, results_title)
pdf("../backprop/tex/nn_assignment4_p2_overview.pdf", height = 8, width = 12)
print(tplot)
dev.off()
tplot

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(tdata, error_history_title)
pdf("../backprop/tex/nn_assignment4_p2_error.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot



test_data <- "problem3_test_0.012_10.csv"
train_data <- "problem3_train_0.012_10.csv"

results_title = "Inverse X Output Training versus Test versus Expected for Given Epoch\n Configuration = 1-10-1, alpha = .012, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-10-1, alpha = .012, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

tdata <- create_merged_data_set(test_data, train_data)
tplot <- create_combined_plot(tdata, results_title)
pdf("../backprop/tex/nn_assignment4_p3_overview1.pdf", height = 8, width = 12)
print(tplot)
dev.off()
tplot

error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/tex/nn_assignment4_p3_error1.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot



test_data <- "problem3_test_0.003_10.csv"
train_data <- "problem3_train_0.003_10.csv"

results_title = "Inverse X Output Training versus Test versus Expected for Given Epoch\n Configuration = 1-10-1, alpha = .003, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-10-1, alpha = .003, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

tdata <- create_merged_data_set(test_data, train_data)
tplot <- create_combined_plot(tdata, results_title)
pdf("../backprop/tex/nn_assignment4_p3_overview2.pdf", height = 8, width = 12)
print(tplot)
dev.off()
tplot

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/tex/nn_assignment4_p3_error2.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem3_test_0.005_20.csv"
train_data <- "problem3_train_0.005_20.csv"

results_title = "Inverse X Output Training versus Test versus Expected for Given Epoch\n Configuration = 1-20-1, alpha = .005, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-20-1, alpha = .005, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

tdata <- create_merged_data_set(test_data, train_data)
tplot <- create_combined_plot(tdata, results_title)
pdf("../backprop/tex/nn_assignment4_p3_overview3.pdf", height = 8, width = 12)
print(tplot)
dev.off()
tplot

error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/tex/nn_assignment4_p3_error3.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot






# plot the steps of each element versus the actual
num_steps <- 5
count <- 0
p2_train_data <- read.csv("problem2_train_results.csv"# 
steps <- unique(p2_train_data$Step)

count <- floor(length(steps) / num_steps)
analysis <- c();
for (i in 1:length(steps)){
   if (i %% count == 0){
      analysis <- append(analysis, steps[i])
   }
}

p <- subset(p2_train_data, Step %in% analysis)
train_zz <- melt(p, id.vars=c("Input", "Step"))

% used later on
train_error <- data.frame(Step=p['Step'], Input = p['Input'], value=p['Error'])
training_hits <- data.frame(Step=p['Step'], Input=p['Input'], value=p['Hit'])
training_hits$Steps <- factor(training_hits$Step, ordered= T)
training_hits <- subset(training_hits, Hit >= 1) 


actuals <- subset(train_zz, variable == "Actual")
desired <- subset(train_zz, variable == "Expected")
actuals$Steps <- factor(actuals$Step, ordered= T)

train_input_output_plot <- ggplot() +  
    geom_line(data=actuals, mapping = aes(x=Input, y=value, color = Steps)) + 
      geom_point(data=actuals, mapping = aes(x=Input, y=value, color = Steps)) + 
      ylab("Neural Network Output Values") + 
      xlab("Neural Network Input Values") + 
      geom_line(data = desired, mapping = aes(x = Input, y = value), color = "black") + 
      geom_rug(data = training_hits, mapping = aes(x = Input, y = NULL, color = Steps)) +
      facet_grid(Step ~ .) +   
  opts(title = "Training versus Expected Output Using Evenly Spaced Monitor Steps\n for a 2-Layer Inverse x Neural Net with 12 PEs and Epoch = 200 (Epochs shown in margin)",
  title.position=c(.5,0.9))

pdf("../backprop/tex/nn_assignment4_p2_overview.pdf", height = 8, width = 12)
print(train_input_output_plot)
dev.off()
train_input_output_plot

#%  plot the test output versuses the expected output

p2_test_data <- read.csv("problem2_test_results.csv", stringsAsFactors=F)

p <- p2_test_data

test_error <- data.frame(Step=p['Step'], Input = p['Input'], value=p['Error'])
test_hits <- data.frame(Step=p['Step'], Input=p['Input'], value=p['Hit'])
test_hits <- subset(test_hits, Hit >= 1) 
train_zz <- melt(p, id.vars=c("Input", "Step"))


actuals <- subset(train_zz, variable == "Actual")
desired <- subset(train_zz, variable == "Expected")
actuals$Steps <- factor(actuals$Step, ordered= T)
input_output_plot <- ggplot() +    
  geom_line(data=actuals, mapping = aes(x=Input, y=value, color = "Actual")) + 
  geom_point(data=actuals, mapping = aes(x=Input, y=value, color = "Actual"))  + 
  ylab("Neural Network Output Values") + xlab("Neural Network Input Values") + 
  geom_line(data = desired, mapping = aes(x = Input, y = value, color = "Desired")) + 
  geom_point(data = desired, mapping = aes(x = Input, y = value, color = "Desired")) + 
  geom_rug(data = test_hits, mapping = aes(x = Input, y = NULL)) +
  scale_color_manual("Test Curve", values = c("Desired" = "black", "Actual" = "red")) +
  opts(title = "Test Output Compared for a 2-Layer Inverse X\nNeural Net with 12 PEs and Epoch of 200",
  title.position=c(.5,0.9)) 


pdf("../backprop/tex/nn_assignment4_p2_test.pdf", height = 8, width = 12)
print(input_output_plot)
dev.off()
input_output_plot


#%  plot the test and Train error


error_plot2 <- ggplot() + 
    geom_line(data=train_error, aes(x=Input, y=Error, group = Step, color=Step)) +
    geom_line(data=test_error, aes(x=Input, y=Error), color = "black") +
    geom_point(data=train_error, aes(x=Input, y=Error, group = Step, color=Step)) +
    geom_point(data=test_error, aes(x=Input, y=Error), color = "black") +
    geom_rug(data = training_hits, mapping = aes(x = Input, y = NULL, color = Step)) +
    geom_rug(data = test_hits, mapping = aes(x = Input, y = NULL)) +
    opts(title = "Error from a 2-Layer Inverse X Neural Net\n with 12 PEs and Epoch of 200",
    title.position=c(.5,0.9))

pdf("../backprop/tex/nn_assignment4_p2_error.pdf", height = 8, width = 12)
print(error_plot2)
dev.off()
error_plot2
