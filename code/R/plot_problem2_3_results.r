library (ggplot2)


scale_term <- function(min_val, max_val){
    return(.5 * (max_val - min_val));
}
scale_up <- function(value, min_val = 1, max_val = 10){
    return( (value / scale_term(min_val, max_val)^-1) + 
           scale_term(min_val, max_val) + 1)
}

rmse <- function(obs, pred) sqrt(mean((obs-pred)^2))


combine_data <- function(test_data, train_data){
    
    p2_test_data <- read.csv(test_data, stringsAsFactors=F)
    p2_train_data <- read.csv(train_data, stringsAsFactors=F)
    
    p2_test_data$ScaledActual <- scale_up(p2_test_data$Actual)
    p2_test_data$ScaledExpected <- scale_up(p2_test_data$Expected)
    p2_test_data$run <- "Test"
    
    p2_train_data$ScaledActual <- scale_up(p2_train_data$Actual)
    p2_train_data$ScaledExpected <- scale_up(p2_train_data$Expected)
    p2_train_data$run <- "Train"
    p2_all <- rbind(p2_train_data, p2_test_data)
    return(p2_all)
}

create_merged_data_set <- function(test_data, train_data){
    
    p2_all <- combine_data(test_data, train_data)
    records <- unique((p2_all$Epoch))
    lrecords <- length(records)
    
    selection <- c(records[1], records[lrecords/4], 
                records[lrecords/2], records[lrecords*.75], 
                records[lrecords])
    
    p2_subset_all <- subset(p2_all, Epoch %in% selection)
    p2_subset_all$value <- p2_subset_all$ScaledActual
    
    p2_expected <- p2_subset_all
    p2_expected$value <- p2_expected$ScaledExpected
    p2_expected$run <- "Expected"
    
    p2_subset_all <- rbind(p2_subset_all, p2_expected)
    return(p2_subset_all)

}

create_combined_plot <- function(data_input, Title){
    data_input$Input
    train_input_output_plot <- ggplot() +  
        geom_line(data=data_input, mapping = aes(x=Input, y=value, linetype = run, color = run)) + 
        geom_point(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
          ylab("Neural Network Output Values") + 
          xlab("Neural Network Input Values") + 
          scale_colour_manual("Data Point Types",  values = c("Expected" = "black", "Train" = "red", "Test" = "blue")) +
          geom_rug(data = subset(data_input, Hit == 1), mapping = aes(x = Input, y = NULL, color = run)) +
          facet_grid(Epoch ~ .) +   
      opts(title = Title,
      title.position=c(.5,0.9))
    return(train_input_output_plot)
}


create_error_hist_plot <- function(data_input, Title){

    data_input <- ddply(data_input, .( Epoch, run ), mutate, 
                     value = rmse(Actual, Expected))
    data_input2 <-  data.frame(Epoch = data_input$Epoch, 
                        value = data_input$value, run = data_input$run)
    
    error_plot <- ggplot(data = data_input2, mapping = aes(x = Epoch, 
        y=value, color = run,  linetype = run )) +  
        geom_line() + 
          geom_point() + 
          ylab("Neural Network Error Values") + 
          xlab("Neural Network Epochs (Epoch Size = 200)") + 
          scale_colour_manual("Data Point Types",  values = c("Train" = "red", "Test" = "blue")) +
      opts(title = Title,
      title.position=c(.5,0.9))
    return(error_plot)
}

test_data <- "problem3_test_0.005_10.csv"
train_data <- "problem3_train_0.005_10.csv"

results_title = "Inverse X Output Training versus Test versus Expected for Given Epoch\n Configuration = 1-10-1, alpha = .005, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-10-1, alpha = .005, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

tdata <- create_merged_data_set(test_data, train_data)
tplot <- create_combined_plot(tdata, results_title)
pdf("../backprop/hw/hw4/nn_assignment4_p2_overview.pdf", height = 8, width = 12)
print(tplot)
dev.off()
tplot

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p2_error.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot



test_data <- "problem3_test_0.012_10.csv"
train_data <- "problem3_train_0.012_10.csv"

results_title = "Inverse X Output Training versus Test versus Expected for Given Epoch\n Configuration = 1-10-1, alpha = .012, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-10-1, alpha = .012, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

tdata <- create_merged_data_set(test_data, train_data)
tplot <- create_combined_plot(tdata, results_title)
pdf("../backprop/hw/hw4/nn_assignment4_p3_overview1.pdf", height = 8, width = 12)
print(tplot)
dev.off()
tplot

error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p3_error1.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot



test_data <- "problem3_test_0.003_10.csv"
train_data <- "problem3_train_0.003_10.csv"

results_title = "Inverse X Output Training versus Test versus Expected for Given Epoch\n Configuration = 1-10-1, alpha = .003, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-10-1, alpha = .003, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

tdata <- create_merged_data_set(test_data, train_data)
tplot <- create_combined_plot(tdata, results_title)
pdf("../backprop/hw/hw4/nn_assignment4_p3_overview2.pdf", height = 8, width = 12)
print(tplot)
dev.off()
tplot

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p3_error2.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem3_test_0.005_20.csv"
train_data <- "problem3_train_0.005_20.csv"

results_title = "Inverse X Output Training versus Test versus Expected for Given Epoch\n Configuration = 1-20-1, alpha = .005, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-20-1, alpha = .005, m = 1000 Learning steps = 20000, Epoch Size = 200\n"

tdata <- create_merged_data_set(test_data, train_data)
tplot <- create_combined_plot(tdata, results_title)
pdf("../backprop/hw/hw4/nn_assignment4_p3_overview3.pdf", height = 8, width = 12)
print(tplot)
dev.off()
tplot

error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p3_error3.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot



