library (ggplot2)


BASE_DIR <- '../../hw/hw5/'
FORMAT <- "problem%d_%s%0.03f_%d.pdf"
OUTFILE <- paste(BASE_DIR, FORMAT, sep='')


scale_term <- function(min_val, max_val){
    return(.5 * (max_val - min_val));
}
scale_up <- function(value, min_val = 1, max_val = 10){
    return( (value / scale_term(min_val, max_val)^-1) + 
           scale_term(min_val, max_val) + 1)
}

missrate <- function(obs, pred) {
     return (sum(obs != pred) / length(obs))

}
missrate2 <- function(hits) {
     return ((length(hits) - sum(hits)) / length(hits))
}



combine_data <- function(train_data, test_data){
    
    p4_test_data <- read.csv(test_data, stringsAsFactors=F)
    p4_train_data <- read.csv(train_data, stringsAsFactors=F)
    
    p4_test_data$run <- "Test"
        
    p4_train_data$run <- "Train"
    
    p4_all <- rbind(p4_train_data, p4_test_data)
    return(p4_all)
}

create_merged_data_set <- function(train_data, test_data){
    
    p4_all <- combine_data( train_data, test_data)
    records <- unique((p4_all$Epoch))
    lrecords <- length(records)
    
    selection <- c(records[1], records[lrecords/4], 
                records[lrecords/2], records[lrecords*.75], 
                records[lrecords])
    
    p4_subset_all <- subset(p4_all, Epoch %in% selection)
    p4_subset_all$value <- p4_subset_all$ActualScaled
    
    p4_expected <- p4_subset_all
    p4_expected$value <- p4_expected$ExpectedScaled
    p4_expected$run <- "Expected"
    
    p4_subset_all <- rbind(p4_subset_all, p4_expected)
    return(p4_subset_all)

}

create_combined_plot <- function(data_input, Title){
    data_input$Input
    train_input_output_plot <- ggplot() +  
        geom_line(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
        geom_point(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
          ylab("Neural Network Output Values") + 
          xlab("Neural Network Input Values") + 
          scale_colour_manual("Data Point Types",  values = c("Expected" = "black", "Train" = "red", "Test" = "blue")) +
          geom_rug(data = subset(data_input, Hit == 1), mapping = aes(x = Input, y = NULL, color = run)) +
          facet_grid(Epoch ~ .) +   
      opts(title = Title,
      title.position=c(.5,0.9))
    return(train_input_output_plot)
}


create_combined_plot <- function(data_input, Title){
    data_input$Input
    train_input_output_plot <- ggplot() +  
        geom_line(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
        geom_point(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
          ylab("Neural Network Output Values") + 
          xlab("Neural Network Input Values") + 
          scale_colour_manual("Data Point Types",  values = c("Expected" = "black", "Train" = "red", "Test" = "blue")) +
          scale_linetype("Data Point Types")+
          geom_rug(data = subset(data_input, Hit == 1), mapping = aes(x = Input, y = NULL, color = run)) +
          facet_grid(Epoch ~ .) +   
      opts(title = Title,
      title.position=c(.5,0.9))
    return(train_input_output_plot)
}


create_combined_iris_plot <- function(data_input, Title){
    data_input$Input
    train_input_output_plot <- ggplot() +  
        geom_line(data=data_input, mapping = aes(x=Input, y=ExpectedScaled, color = ExpectedScaled)) + 
        geom_shape(data=data_input, mapping = aes(x=Input, y=value, shape = Hit)) + 
          ylab("Neural Network Output Values") + 
          xlab("Neural Network Input Values") + 
          scale_colour_manual("Data Point Types",  values = c("Expected" = "black", "Train" = "red", "Test" = "blue")) +
          geom_rug(data = subset(data_input, Hit == 1), mapping = aes(x = Input, y = NULL, color = run)) +
          facet_grid(Epoch ~ .) +   
      opts(title = Title,
      title.position=c(.5,0.9))
    return(train_input_output_plot)
}



create_error_hist_plot <- function(data_input, Title){

    data_input <- ddply(data_input, .( Epoch, run ), summarise, value = missrate2(Hit))
    data_input2 <-  data.frame(Epoch = data_input$Epoch, 
                        value = data_input$value, run = data_input$run)
    
    error_plot <- ggplot(data = data_input2, mapping = aes(x = Epoch, 
        y=(value), color = run, linetype = run)) +  
        geom_line() + scale_linetype("Data Point Types")+
          geom_point() + 
          ylab("Neural Network Error Values") + 
          xlab("Neural Network Epochs (Epoch Size = 75)") + 
          scale_colour_manual("Data Point Types",  values = c("Train" = "red", "Test" = "blue")) +
      opts(title = Title,
      title.position=c(.5,0.9))
    return(error_plot)
}

plot_error_iris <- function(iris, title, fname){
    error_plot <- create_error_hist_plot(combine_data(iris[2], iris[1]), title)
    pdf(fname, height = 8, width = 12)
    print(error_plot)
    dev.off()
    error_plot
}


error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-%d-3, alpha = %0.03f, m = 30 Learning steps = 3000, Epoch Size = 75\n"
infile_fmt <- "../../output/problem%d_%s_a1-%0.03f_a2-%0.03f_m1-0.000_m2-0.000_%d.csv"

trainning_title_fmt <- "Training Blablah"
test_title_fmt <- "Training Blablah"


error_titles <- list()
infiles <- list()
outfiles <- list()
problem <- 51

compare_files <- list()
compare_titles <- list()


cnt = 0
hiddenpes <- c(4 , 5, 6)
alphas <- c(.005, .008, .012, .05, .1, .13)
for (hiddenpe in hiddenpes){
    for (alpha in alphas){
        title <- sprintf(error_history_title, hiddenpe, alpha)
        outfile <- sprintf(OUTFILE, problem, "", alpha, hiddenpe)
        infile0 <- sprintf(infile_fmt, problem, "train", alpha,  alpha, hiddenpe)
        infile1 <- sprintf(infile_fmt, problem, "test", alpha,  alpha, hiddenpe)
        error_titles <- append(error_titles, title)
        
        testfile <- sprintf(OUTFILE, problem, "_test_output", alpha, hiddenpe)
        trainfile <- sprintf(OUTFILE, problem, "_train_output", alpha, hiddenpe)
        compare_files <- append(infiles, list(list(trainfile, testfile)))
        
        test_title <- sprintf(test_title_fmt)
        train_title <- sprintf(train_title_fmt)
        compare_titles <- append(infiles, list(list(train_title, test_title)))
        
        infiles <- append(infiles, list(list(infile0, infile1)))
        outfiles <- append(outfiles, outfile)
        cnt = cnt + 1
    }
}

for (i in 1:cnt){
  test <- infiles[[i]][[2]]
  train <- infiles[[i]][[1]]
  outfile <- outfiles[[i]]
  title <- error_titles[[i]]
  plot_error_iris(c(train, test), title, outfile)
}

create_iris_plot <- function( data_input, fname, Title){
    pattern_id <- data.frame(Pattern = seq(1, nrow(data_input)))
    
    data_input <- cbind(pattern_id, data_input)
    compare_plot <- ggplot( data = data_input, mapping = aes(x=Pattern, 
        y=ExpectedScaled, shape = ExpectedScaled) )+  
        geom_line(data=data_input, mapping = aes(group = ExpectedScaled)) +
        geom_point(data=data_input, mapping = aes(color = Hit), size = 5) + 
        scale_shape_discrete(name = "Iris Type", breaks = c(1, 2, 3), 
        limits = c(1, 2, 3), labels = c("Virginica", "Versicolor", "Setosa")) +
        scale_color_discrete(name = "Classification", breaks = c(0, 1), 
        limits = c(0, 1), labels = c("Incorrect", "Correct")) + 
        scale_y_discrete(name="Iris Types", expand=c(0.05, 1), breaks = c(1, 2, 3), 
        limits = c(1, 2, 3), labels = c("Virginica", "Versicolor", "Setosa")) +
        xlab("Pattern Value") + 
        opts(title = Title,
        title.position=c(.5,0.9))
    
    pdf(fname, height = 8, width = 12)
    print(compare_plot)
    dev.off()
    
    
    return (compare_plot)
}

train <- "../../output/problem51_train_a1-0.008_a2-0.008_m1-0.000_m2-0.000_4.csv"
test <- "../../output/problem51_test_a1-0.008_a2-0.008_m1-0.000_m2-0.000_4.csv"
epoch <- 98
fname1 <- "../../hw/hw5/problem51_0.008_4_training_results.pdf"
fname2 <- "../../hw/hw5/problem51_0.008_4_test_results.pdf"
Title1 <- "Training Output for a 4-4-3 Iris Classification ANN at Epoch = 98 with a Learning Rate of .008 (Correct = %d)"
Title2 <- "Test Output for a 4-4-3 Iris Classification ANN at Epoch = 98 with a Learning Rate of .008 (Correct = %d)"


combined_data <- combine_data(train, test)
data_input <- subset(combined_data, run == "Train" & Epoch == epoch)
num_hits <- sum(data_input$ExpectedScaled == data_input$ActualScaled  )

f <- sprintf(Title1, num_hits)
create_iris_plot( data_input, fname1, f)


data_input <- subset(combined_data, run == "Test" & Epoch == epoch)
num_hits <- sum(data_input$ExpectedScaled == data_input$ActualScaled  )
f <- sprintf(Title2, num_hits)
create_iris_plot(data_input, fname2, f)











