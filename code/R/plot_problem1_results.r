library (ggplot2)

p1_data <- read.csv("problem1_results.csv", stringsAsFactors=F)
names(p1_data) <- c("Step", "Input", "Actual", "Hardlim", "Expected", "Error")

expected <- data.frame(Step=p1_data['Step'], value=p1_data['Expected'])
actual <- data.frame(Step=p1_data['Step'], value=p1_data['Actual'])
rms <- data.frame(Step=p1_data['Step'], value=p1_data['Error'])
hardlim <- data.frame(Step=p1_data['Step'], value=p1_data['Hardlim'])

zz <- NULL
zz <- melt(list(Error=rms,Actual=actual,Hardlim=hardlim,Expected=expected), id.vars="Step")


values = c("Expected" = "black", "Actual" = "darkgreen", 
        "Hardlim" = "red", "RMS Error" = "blue")

equal_data <- subset(zz, L1 == "Expected" | L1 == "Hardlim")
equal_data$value <-  subset(equal_data, L1 == "Expected")$value == subset(equal_data, L1 == "Hardlim")$value


input_history <-  ggplot(data = p1_data) + geom_point(mapping = aes(x = Step, 
        y = Actual, group = Input, color = factor(Input))) + 
        geom_line(mapping = aes(x = Step, y = Actual, group = Input, 
        color = factor(Input))) + 
        ylab("Training Output") + scale_color_manual("Input Values`", 
       labels = c("1" = "00", "2" = "01", 
        "3" = "10", "4" = "11"),
       values = c("1" = "black", "2" = "green", 
        "3" = "red", "4" = "blue")) + #facet_grid( Input ~ .) +
        scale_x_continuous(limits = c(0, max(p1_data$Step))) +
        scale_y_continuous(limits = c(-1.1, 1.1)) +
        opts(title = "Actual Values for a 3-2-1 XOR Neural Net and Epoch of 1", title.position=c(.5,0.9))
input_history

input_history2 <-  ggplot(data = p1_data) + geom_point(mapping = aes(x = Step, 
        y = Actual, group = Input, color = factor(Input))) + 
        geom_line(mapping = aes(x = Step, y = Actual, group = Input, 
        color = factor(Input))) +
        geom_rug(data = subset(p1_data, Expected == Hardlim), 
        mapping=aes(x = Step, y = NULL))+
        ylab("Training Output") + scale_color_manual("Input Values`", 
       labels = c("1" = "00", "2" = "01", 
        "3" = "10", "4" = "11"),
       values = c("1" = "black", "2" = "green", 
        "3" = "red", "4" = "blue")) + facet_wrap( ~ Input, ncol = 2) +
        scale_x_continuous(limits = c(0, max(p1_data$Step))) +
        scale_y_continuous(limits = c(-1.1, 1.1)) +
        opts(title = "Actual Values for a 3-2-1 XOR Neural Net and Epoch of 1", title.position=c(.5,0.9))
input_history2



error_plot2 <-  ggplot(data = p1_data) + geom_line(mapping = aes(x = Step, 
        y = Error, group = Input, color = factor(Input))) + 
        geom_rug(data = subset(p1_data, Expected == Hardlim), 
        mapping=aes(x = Step, y = NULL))+
        ylab("Training Error") + scale_color_manual("Input Values`", 
       labels = c("1" = "00", "2" = "01", 
        "3" = "10", "4" = "11"),
       values = c("1" = "black", "2" = "green", 
        "3" = "red", "4" = "blue")) + facet_grid( Input ~ .) +
        scale_x_continuous(limits = c(0, max(p1_data$Step))) +
        scale_y_continuous(limits = c(-1.1, max(p1_data$Error))) +
        opts(title = "Training Error History for a 3-2-1 XOR Neural Net and Epoch of 1", title.position=c(.5,0.9))
error_plot2
    
pdf("../backprop/tex/nn_assignment4_p1_error.pdf", height = 8, width = 12)
print(error_plot2)
dev.off()

pdf("../backprop/tex/nn_assignment4_p1_actual.pdf", height = 8, width = 12)
print(input_history)
dev.off()

pdf("../backprop/tex/nn_assignment4_p1_actual2.pdf", height = 8, width = 12)
print(input_history2)
dev.off()