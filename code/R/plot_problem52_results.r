library (ggplot2)


BASE_DIR <- '../../hw/hw5/'
FORMAT <- "problem%d_%s%0.03f_%d.pdf"
OUTFILE <- paste(BASE_DIR, FORMAT, sep='')
PROBLEM <- 52

scale_term <- function(min_val, max_val){
    return(.5 * (max_val - min_val));
}
scale_up <- function(value, min_val = 1, max_val = 10){
    return( (2 * value))
}

missrate <- function(obs, pred) {
     return (sum(obs != pred) / length(obs))

}
missrate2 <- function(hits) {
     return ((length(hits) - sum(hits)) / length(hits))
}


rmse <- function(obs, pred) sqrt(mean((obs-pred)^2))

combine_data <- function(train_data, test_data){
    
    p4_test_data <- read.csv(test_data, stringsAsFactors=F)
    p4_train_data <- read.csv(train_data, stringsAsFactors=F)
    
    p4_test_data$run <- "Test"
        
    p4_train_data$run <- "Train"
    
    p4_all <- rbind(p4_train_data, p4_test_data)
    return(p4_all)
}

combine_three_data <- function(train_data, test_data, test2_data = NULL){
    
    p4_test_data <- read.csv(test_data, stringsAsFactors=F)
    
    p4_train_data <- read.csv(train_data, stringsAsFactors=F)
    p4_test_data$run <- "Test 1"
    p4_train_data$run <- "Train"
    
    p4_all <- NULL   
    if (length(test2_data) > 0){
       p4_test2_data <- read.csv(test2_data, stringsAsFactors=F)
       p4_test2_data$run <- "Test 2"
       p4_all <- rbind(p4_train_data, p4_test_data, p4_test2_data)
    }else{
    	p4_all <- rbind(p4_train_data, p4_test_data)
    }
    return(p4_all)
}





create_merged_data_set <- function(train_data, test_data, test2_data = NULL){
    
    p4_all <- combine_data( train_data, test_data, test2_data)
    records <- unique((p4_all$Epoch))
    lrecords <- length(records)
    
    selection <- c(records[1], records[lrecords/4], 
                records[lrecords/2], records[lrecords*.75], 
                records[lrecords])
    
    p4_subset_all <- subset(p4_all, Epoch %in% selection)
    p4_subset_all$value <- p4_subset_all$ActualScaled
    
    p4_expected <- p4_subset_all
    p4_expected$value <- p4_expected$ExpectedScaled
    p4_expected$run <- "Expected"
    
    p4_subset_all <- rbind(p4_subset_all, p4_expected)
    return(p4_subset_all)

}

create_combined_plot <- function(data_input, Title){
    data_input$Input
    train_input_output_plot <- ggplot() +  
        geom_line(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
        geom_point(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
          ylab("Neural Network Output Values") + 
          xlab("Neural Network Input Values") + 
          scale_colour_manual("Data Point Types",  values = c("Expected" = "black", "Train" = "red", "Test" = "blue")) +
          geom_rug(data = subset(data_input, Hit == 1), mapping = aes(x = Input, y = NULL, color = run)) +
          facet_grid(Epoch ~ .) +   
      opts(title = Title,
      title.position=c(.5,0.9))
    return(train_input_output_plot)
}


create_combined_plot <- function(data_input, Title){
    data_input$Input
    train_input_output_plot <- ggplot() +  
        geom_line(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
        geom_point(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
          ylab("Neural Network Output Values") + 
          xlab("Neural Network Input Values") + 
          scale_colour_manual("Data Point Types",  values = c("Expected" = "black", "Train" = "red", "Test" = "blue")) + scale_linetype("Data Point Types")+
          geom_rug(data = subset(data_input, Hit == 1), mapping = aes(x = Input, y = NULL, color = run)) +
          facet_grid(Epoch ~ .) +   
      opts(title = Title,
      title.position=c(.5,0.9))
    return(train_input_output_plot)
}



create_error_hist_plot <- function(data_input, Title, fname){
    data_inputk <- ddply(data_input, .( Epoch, run ), mutate, 
                     value = rmse(ActualScaled, ExpectedScaled))
    data_input2 <-  data.frame(Epoch = data_inputk$Epoch, 
                        value = data_inputk$value, run = data_inputk$run)
        
    error_plot <- ggplot(data = unique(data_input2), mapping = aes(x = (Epoch), 
        y=(value), color = run,  linetype = run, group = run)) +  
        geom_line() + 
          geom_point() + scale_linetype("Data Point Types")+
          ylab("Neural Network Error Values, RMS") + 
          xlab("Neural Network Epochs in Log Base 2 (Epoch Size = 200)") + 
          scale_colour_manual("Data Point Types",  values = c("Train" = "red", "Test 1" = "blue", "Test 2" = "darkgreen" )) +
      opts(title = Title,
      title.position=c(.5,0.9))
        pdf(fname, height = 8, width = 12)
    print(error_plot)
    dev.off()

    return(error_plot)
}


print_bad_output <- function(d, epoch, fname, title){
    
    compare_plot <- ggplot() + geom_point(data = subset(d, Epoch == epoch), 
    mapping = aes(x = Input, y = ActualScaled, color = "red")) + 
    geom_line(data = subset(d, Epoch == epoch), 
    mapping = aes(x = Input, y = ActualScaled, color = "red")) +
    geom_line(data = subset(d, Epoch == epoch), 
    mapping = aes(x = Input, y = ExpectedScaled, color = "blue")) +    
    geom_point(data = subset(d, Epoch == epoch), 
    mapping = aes(x = Input, y = ExpectedScaled, color = "blue")) + facet_grid( . ~ run) +
    ylab("Output Values of the ANN") + 
    xlab("Input Values to the ANN") + scale_linetype("Data Point Types")+
    scale_colour_manual("Type of Output Value",  
        labels = c("red" = "Actual", "blue" = "Desired"),
        values = c("red" = "red", "blue" = "blue")) + 
    opts(title = title, title.position=c(.5,0.9))
    
    pdf(fname, height = 8, width = 12)
    print(compare_plot)
    dev.off()
    
    
    return (compare_plot)

}





error_history_title = "Error History Training versus Test by Epoch\n Configuration = 1-%d-1, alpha = %0.03f, m = 2000 Learning steps = 2x10^5, Epoch Size = 120\n"
infile_fmt <- "../../output/problem%d_%s_a1-%0.03f_a2-%0.03f_m1-0.000_m2-0.000_%d.csv"

problem <- 52
cnt = 0
hiddenpe <- c(80)
alpha <- c(.008)

training <- sprintf(infile_fmt, PROBLEM, "train", alpha,  alpha, hiddenpe)
test <- sprintf(infile_fmt, PROBLEM, "test", alpha,  alpha, hiddenpe)
test2 <- sprintf(infile_fmt, PROBLEM, "test2", alpha,  alpha, hiddenpe)
etitle <- sprintf(error_history_title, hiddenpe, alpha)
outfile <- sprintf(OUTFILE, problem, "error", alpha, hiddenpe)
datainput <- combine_three_data(training, test, test2)

outfile_comp <- "../../hw/hw5/problem52_0.008_80_comparison.pdf"
ctitle <- "Point plots of the Test 1, Test 2, and the Training Data\nfor a 1-80-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5 Learning Steps"
print_bad_output(datainput, 2352, outfile_comp, ctitle)
create_error_hist_plot(datainput, etitle, outfile)
create_error_hist_plot(datainput, etitle, outfile)

hiddenpe <- c(80)
alpha <- c(.01)

training <- sprintf(infile_fmt, PROBLEM, "train", alpha,  alpha, hiddenpe)
test <- sprintf(infile_fmt, PROBLEM, "test", alpha,  alpha, hiddenpe)
test2 <- sprintf(infile_fmt, PROBLEM, "test2", alpha,  alpha, hiddenpe)
etitle <- sprintf(error_history_title, hiddenpe, alpha)
outfile <- sprintf(OUTFILE, problem, "error", alpha, hiddenpe)
datainput <- combine_three_data(training, test, test2)

outfile_comp <- "../../hw/hw5/problem52_0.010_80_comparison.pdf"
ctitle <- "Point plots of the Test 1, Test 2, and the Training Data\nfor a 1-80-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5 Learning Steps"
print_bad_output(datainput, 2352, outfile_comp, ctitle)
create_error_hist_plot(datainput, etitle, outfile)




hiddenpe <- c(4)
alpha <- c(.008)

training <- sprintf(infile_fmt, PROBLEM, "train", alpha,  alpha, hiddenpe)
test <- sprintf(infile_fmt, PROBLEM, "test", alpha,  alpha, hiddenpe)
test2 <- sprintf(infile_fmt, PROBLEM, "test2", alpha,  alpha, hiddenpe)
etitle <- sprintf(error_history_title, hiddenpe, alpha)
outfile <- sprintf(OUTFILE, problem, "error", alpha, hiddenpe)
datainput <- combine_three_data(training, test, test2)

outfile_comp <- "../../hw/hw5/problem52_0.008_4_comparison.pdf"
ctitle <- "Point plots of the Test 1, Test 2, and the Training Data\nfor a 1-4-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5 Learning Steps"
print_bad_output(datainput, 2352, outfile_comp, ctitle)
create_error_hist_plot(datainput, etitle, outfile)

hiddenpe <- c(4)
alpha <- c(.01)

training <- sprintf(infile_fmt, PROBLEM, "train", alpha,  alpha, hiddenpe)
test <- sprintf(infile_fmt, PROBLEM, "test", alpha,  alpha, hiddenpe)
test2 <- sprintf(infile_fmt, PROBLEM, "test2", alpha,  alpha, hiddenpe)
etitle <- sprintf(error_history_title, hiddenpe, alpha)
outfile <- sprintf(OUTFILE, problem, "error", alpha, hiddenpe)
datainput <- combine_three_data(training, test, test2)



outfile_comp <- "../../hw/hw5/problem52_0.010_4_comparison.pdf"
ctitle <- "Point plots of the Test 1, Test 2, and the Training Data\nfor a 1-4-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5 Learning Steps"
print_bad_output(datainput, 2352, outfile_comp, ctitle)
create_error_hist_plot(datainput, etitle, outfile)










hiddenpe <- c(80)
alpha <- c(.008)

training <- sprintf(infile_fmt, PROBLEM, "train_2", alpha,  alpha, hiddenpe)
test <- sprintf(infile_fmt, PROBLEM, "test_2", alpha,  alpha, hiddenpe)
test2 <- sprintf(infile_fmt, PROBLEM, "test2_2", alpha,  alpha, hiddenpe)
etitle <- sprintf(error_history_title, hiddenpe, alpha)
outfile <- sprintf(OUTFILE, problem, "error_2", alpha, hiddenpe)
datainput <- combine_three_data(training, test, test2)

outfile_comp <- "../../hw/hw5/problem52_0.008_80_comparison_2.pdf"
ctitle <- "Point plots of the Test 1, Test 2, and the Training Data\nfor a 1-80-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5 Learning Steps"
print_bad_output(datainput, 2352, outfile_comp, ctitle)
create_error_hist_plot(datainput, etitle, outfile)
create_error_hist_plot(datainput, etitle, outfile)

hiddenpe <- c(80)
alpha <- c(.01)

training <- sprintf(infile_fmt, PROBLEM, "train_2", alpha,  alpha, hiddenpe)
test <- sprintf(infile_fmt, PROBLEM, "test_2", alpha,  alpha, hiddenpe)
test2 <- sprintf(infile_fmt, PROBLEM, "test2_2", alpha,  alpha, hiddenpe)
etitle <- sprintf(error_history_title, hiddenpe, alpha)
outfile <- sprintf(OUTFILE, problem, "error_2", alpha, hiddenpe)
datainput <- combine_three_data(training, test, test2)

outfile_comp <- "../../hw/hw5/problem52_0.010_80_comparison_2.pdf"
ctitle <- "Point plots of the Test 1, Test 2, and the Training Data\nfor a 1-80-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5 Learning Steps"
print_bad_output(datainput, 2352, outfile_comp, ctitle)
create_error_hist_plot(datainput, etitle, outfile)




hiddenpe <- c(4)
alpha <- c(.008)

training <- sprintf(infile_fmt, PROBLEM, "train_slope1", alpha,  alpha, hiddenpe)
test <- sprintf(infile_fmt, PROBLEM, "test_slope1", alpha,  alpha, hiddenpe)
test2 <- sprintf(infile_fmt, PROBLEM, "test2_slope1", alpha,  alpha, hiddenpe)
etitle <- sprintf(error_history_title, hiddenpe, alpha)
outfile <- sprintf(OUTFILE, problem, "error_slope1", alpha, hiddenpe)
datainput <- combine_three_data(training, test, test2)

outfile_comp <- "../../hw/hw5/problem52_0.008_4_comparison_2.pdf"
ctitle <- "Point plots of the Test 1, Test 2, and the Training Data\nfor a 1-4-1 ANN at Epoch = 2352 with and Epoch of 2000 and 2x10^5 Learning Steps"
print_bad_output(datainput, 2352, outfile_comp, ctitle)
create_error_hist_plot(datainput, etitle, outfile)

hiddenpe <- c(4)
alpha <- c(.01)

training <- sprintf(infile_fmt, PROBLEM, "train_slope1", alpha,  alpha, hiddenpe)
test <- sprintf(infile_fmt, PROBLEM, "test_slope1", alpha,  alpha, hiddenpe)
test2 <- sprintf(infile_fmt, PROBLEM, "test2_slope1", alpha,  alpha, hiddenpe)
etitle <- sprintf(error_history_title, hiddenpe, alpha)
outfile <- sprintf(OUTFILE, problem, "error_slope1", alpha, hiddenpe)
datainput <- combine_three_data(training, test, test2)



outfile_comp <- "../../hw/hw5/problem52_0.010_4_comparison_2.pdf"
ctitle <- "Point plots of the Test 1, Test 2, and the Training Data\nfor a 1-4-1 ANN at Epoch = 1666 with and Epoch of 2000 and 2x10^5 Learning Steps"
print_bad_output(datainput, 2352, outfile_comp, ctitle)
create_error_hist_plot(datainput, etitle, outfile)





