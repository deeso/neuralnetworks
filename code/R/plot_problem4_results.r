library (ggplot2)


scale_term <- function(min_val, max_val){
    return(.5 * (max_val - min_val));
}
scale_up <- function(value, min_val = 1, max_val = 10){
    return( (value / scale_term(min_val, max_val)^-1) + 
           scale_term(min_val, max_val) + 1)
}

missrate <- function(obs, pred) {
     return (sum(obs != pred) / length(obs))

}
missrate2 <- function(hits) {
     return ((length(hits) - sum(hits)) / length(hits))
}



combine_data <- function(test_data, train_data){
    
    p4_test_data <- read.csv(test_data, stringsAsFactors=F)
    p4_train_data <- read.csv(train_data, stringsAsFactors=F)
    
    p4_test_data$run <- "Test"
        
    p4_train_data$run <- "Train"
    
    p4_all <- rbind(p4_train_data, p4_test_data)
    return(p4_all)
}

create_merged_data_set <- function(test_data, train_data){
    
    p4_all <- combine_data(test_data, train_data)
    records <- unique((p4_all$Epoch))
    lrecords <- length(records)
    
    selection <- c(records[1], records[lrecords/4], 
                records[lrecords/2], records[lrecords*.75], 
                records[lrecords])
    
    p4_subset_all <- subset(p4_all, Epoch %in% selection)
    p4_subset_all$value <- p4_subset_all$ActualScaled
    
    p4_expected <- p4_subset_all
    p4_expected$value <- p4_expected$ExpectedScaled
    p4_expected$run <- "Expected"
    
    p4_subset_all <- rbind(p4_subset_all, p4_expected)
    return(p4_subset_all)

}

create_combined_plot <- function(data_input, Title){
    data_input$Input
    train_input_output_plot <- ggplot() +  
        geom_line(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
        geom_point(data=data_input, mapping = aes(x=Input, y=value, color = run)) + 
          ylab("Neural Network Output Values") + 
          xlab("Neural Network Input Values") + 
          scale_colour_manual("Data Point Types",  values = c("Expected" = "black", "Train" = "red", "Test" = "blue")) +
          geom_rug(data = subset(data_input, Hit == 1), mapping = aes(x = Input, y = NULL, color = run)) +
          facet_grid(Epoch ~ .) +   
      opts(title = Title,
      title.position=c(.5,0.9))
    return(train_input_output_plot)
}


create_error_hist_plot <- function(data_input, Title){

    data_input <- ddply(data_input, .( Epoch, run ), summarise, value = missrate2(Hit))
    data_input2 <-  data.frame(Epoch = data_input$Epoch, 
                        value = data_input$value, run = data_input$run)
    
    error_plot <- ggplot(data = data_input2, mapping = aes(x = Epoch, 
        y=value, color = run, linetype = run)) +  
        geom_line() + 
          geom_point() + 
          ylab("Neural Network Error Values") + 
          xlab("Neural Network Epochs (Epoch Size = 30)") + 
          scale_colour_manual("Data Point Types",  values = c("Train" = "red", "Test" = "blue")) +
      opts(title = Title,
      title.position=c(.5,0.9))
    return(error_plot)
}

get_class <- function(data_points){


}


test_data <- "problem4_test_0.050_10.csv"
train_data <- "problem4_train_0.050_10.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-10-3, alpha = .05, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_050_10.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.005_10.csv"
train_data <- "problem4_train_0.005_10.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-10-3, alpha = .005, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_0050_10.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.008_10.csv"
train_data <- "problem4_train_0.008_10.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-10-3, alpha = .008, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_008_10.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.012_10.csv"
train_data <- "problem4_train_0.012_10.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-10-3, alpha = .012, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_012_10.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.100_10.csv"
train_data <- "problem4_train_0.100_10.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-10-3, alpha = .10, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_100_10.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

#%%%%%%%%%% start 20 nodes
test_data <- "problem4_test_0.050_20.csv"
train_data <- "problem4_train_0.050_20.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-20-3, alpha = .05, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error0.050_20.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.005_20.csv"
train_data <- "problem4_train_0.005_20.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-20-3, alpha = .005, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_005_20.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.008_20.csv"
train_data <- "problem4_train_0.008_20.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-20-3, alpha = .008, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_008_20.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.012_20.csv"
train_data <- "problem4_train_0.012_20.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-20-3, alpha = .012, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_012_20.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.100_20.csv"
train_data <- "problem4_train_0.100_20.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-20-3, alpha = .10, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_100_20.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot


#%%%%%%%%%% start 5 nodes
test_data <- "problem4_test_0.050_5.csv"
train_data <- "problem4_train_0.050_5.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-5-3, alpha = .05, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_050_05.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.005_5.csv"
train_data <- "problem4_train_0.005_5.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-5-3, alpha = .005, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_005_05.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.008_5.csv"
train_data <- "problem4_train_0.008_5.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-5-3, alpha = .008, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_008_05.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.012_5.csv"
train_data <- "problem4_train_0.012_5.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-5-3, alpha = .012, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_012_05.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot

test_data <- "problem4_test_0.100_5.csv"
train_data <- "problem4_train_0.100_5.csv"

error_history_title = "Error History Training versus Test by Epoch\n Configuration = 4-5-3, alpha = .10, m = 30 Learning steps = 3000, Epoch Size = 30\n"

datap <- combine_data(test_data, train_data)
error_plot <- create_error_hist_plot(combine_data(test_data, train_data), error_history_title)
pdf("../backprop/hw/hw4/nn_assignment4_p4_error_100_05.pdf", height = 8, width = 12)
print(error_plot)
dev.off()
error_plot


